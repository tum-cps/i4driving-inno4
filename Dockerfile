# Base image is from anaconda guys, so anaconda itself is already installed
FROM continuumio/anaconda3:2023.03-1


# Neccesary basics
RUN apt-get update && \
    apt-get install -y \
    git \
    build-essential \
    cmake \
    clang clang-tidy clang-format \
    libblas3 liblapack3 liblapack-dev libblas-dev gfortran \
    libboost-dev \
    libboost-thread-dev \
    libboost-test-dev \
    libboost-filesystem-dev \
    libeigen3-dev \
    libomp-dev \
    libcgal-dev \
    libyaml-cpp-dev \
    libgl1 \
    nano\
    vim\
    doctest-dev && \
    rm -rf /var/lib/apt/lists/*



# create conda env
RUN conda create -n inno4 python=3.10


# Setup structure
WORKDIR /home
RUN mkdir inno4
WORKDIR /home/inno4

# Copy local dir 
COPY . /home/inno4/

# install commonroad drivability
WORKDIR /home/inno4/commonroad-drivability-checker
RUN conda run -n inno4 bash build.sh -j 4 --cgal -i


# install commonroad reach
WORKDIR /home/inno4/commonroad-reachable-set 
RUN conda run -n inno4 pip install -r requirements.txt
RUN apt update -y && \
    apt upgrade -y libomp-dev && \
    mkdir build 

WORKDIR /home/inno4/commonroad-reachable-set/build
RUN cmake -DCRDC_DIR=/home/inno4/commonroad-drivability-checker -DPYTHON_VER=310 .. && \
    cmake --build . && \
    mkdir temp.linux-x86_64-cpython-310 && \
    cp -r cpp/ temp.linux-x86_64-cpython-310/

WORKDIR /home/inno4/commonroad-reachable-set
RUN cp pycrreach.*.so commonroad_reach/


# install commonroad reach semantic
WORKDIR /home/inno4/commonroad-reach-semantic
RUN conda run -n inno4 pip install -r requirements.txt && \
    mkdir -p /etc/apt/keyrings && \
    conda install -y -n inno4 -c conda-forge spot && \
    wget -q -O - https://www.lrde.epita.fr/repo/debian.gpg | tee /etc/apt/keyrings/lrde-spot.gpg && \
    echo 'deb [signed-by=/etc/apt/keyrings/lrde-spot.gpg] http://www.lrde.epita.fr/repo/debian/ stable/' | tee -a /etc/apt/sources.list && \
    apt-get update -y && \
    apt-get install -y libspot-dev && \
    mkdir build

WORKDIR /home/inno4/commonroad-reach-semantic/build
RUN cmake -DCRDC_DIR=/home/inno4/commonroad-drivability-checker -DCRREACH_DIR=/home/inno4/commonroad-reachable-set -DPYTHON_VER=310 -DCMAKE_BUILD_TYPE=Release .. && \
    cmake --build .

WORKDIR /home/inno4/commonroad-reach-semantic
RUN mv pycrreachs.*.so commonroad_reach_semantic/


# Currently, we do not use commonroad-search, so it is not installed


# install cvxopt and cvxpy
WORKDIR /home/inno4/i4crit
RUN conda run -n inno4 pip install cvxopt cvxpy

# add cr-reach and cr-reach-semantic to conda pythonpath analogous (!WARNING conda does not use PYTHONPATH itself, so export wont work)
RUN conda develop -n inno4 /home/inno4/commonroad-reachable-set /home/inno4/commonroad-reach-semantic






