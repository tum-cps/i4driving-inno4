# i4Driving Critical Scenario Generation and Criticality Enhancement
This repo contains one approach for the i4driving project Working Package 3.2 of TUM-Althoff. **Note that it requires Ubuntu and Python 3.10**. **Important**: this repo uses git submodules, so do not forget to use `git submodule update --init --recursive` after each fetch or pull of the submodules.


***
# Working with the Code
The code for criticality enhancement and critical scenario generation is located in [/i4crit](/i4crit/). In there, you can also find a [script for testing the installation](/i4crit/test_installation.py).



***
# 1. Installation
This section describes the installation.
- It is strongly recommended to use the [anaconda environment and package manager](https://www.anaconda.com/)
- It is recommended to use the dev install (and not pip) for the submodules

## 1.1 Dependencies

### OS
This project was tested only for Ubuntu, not for Windows or MacOS.


### Submodules
This projects uses several [CommonRoad](https://commonroad.in.tum.de/getting-started) modules.
- **commonroad-reachable-set (cr-rs)**: A toolbox for working with reachable sets in autonomous driving. See [here](https://commonroad.in.tum.de/docs/commonroad-reach/)
- **commonroad-reach-semantic (cr-semantic)**: Enhancement of commonroad-reachable-set with Linear Temporal Logic for traffic rules. This package is still work-in-progress and not published yet.
- **commonroad-drivability-checker (cr-dc)**: Checks drivability for reachable sets considering obstacles and road bounds. See [here](https://cps.pages.gitlab.lrz.de/commonroad-drivability-checker/).
- **commonroad-search**: For sampling-based VUT dummy planner


### Third party system-wide libraries
The commonroad submodules use third-party libraries. Please refer to their installation guide for an up-to-date list.

## 1.2 Installation Steps
1. clone this repository
2. in the root, init the submodules: `git submodule update --init --recursive`
3. Activate your conda environment
4. In its respective submodule, install cr-dc via bash-script, like [here](https://cps.pages.gitlab.lrz.de/commonroad-drivability-checker/installation.html#method-1-automatic-installation-via-bash-script).
    - use the bash installation script and the number of jobs you want to use (e.g. 4): `bash build.sh -j JOB_COUNT --cgal -i`
5. In its respective submodule, install cr-rs via dev installation (not pip)
    - Follow the building process [here](https://gitlab.lrz.de/cps/commonroad-reachable-set/-/tree/i4driving?ref_type=heads#building-the-code). If you run into problems with the, consule the [README for dev](https://gitlab.lrz.de/cps/commonroad-reachable-set/-/blob/i4driving/README_FOR_DEVS.md) but **DO NOT USE the last pip install command** 
    - build the source as described in the README_for_devs. 
    - Remember to move the `pycrreach.*.so` in the commonroad_reach folder of the submodule
    - in the `/build` folder, create a folder named `temp.linux-x86_64-cpython-310` and copy the folder `/build/cpp/ into it`.
6. In its respective submodule, install cr-semantic via dev installation:
    - Follow the building process [here](https://gitlab.lrz.de/cps/commonroad/commonroad-reach-semantic/-/tree/i4driving?ref_type=heads#building-the-code).
    - Remember to move the `*.so` into commonroad_reach_semantic of the submodule.
7. In its respective submodule, install cr-search: 
    - Follow the installatino process [here](https://gitlab.lrz.de/cps/commonroad-search)
    - The requirements.txt install might throw a weird error.
8. In your IDE, use [i4crit](/i4crit/), [commonroad-reachable-set](), [commonroad-reach-semantic](/commonroad-reach-semantic/), [commonroad-search](/commonroad-search/) as your root folders.
    - PyCharm: Preference -> Project -> Project Structe -> Add root folder
9. Make sure that your conda env **DOES NOT** use a PyPi installation of cr-rs and cr-semantic
    - `conda list` and `pip list` show all packages. Make sure that there is **no commonroad-reach** in those lists.
10. In your conda environment use `pip install cvxopt` and afterwards `pip install cvxpy`
11. After you are finished setting up everything, run the [test_installation.py](/i4crit/test_installation.py) script to test the installation. This implicitly uses PyTest.


***
# Troubleshooting
## Installation
- **cpp built fails, python import fails ...**:
    - check, if you used `git submodule update --init --recursive`. When using MacOS, non-trivial changes to the CMakeFiles etc. are necessary.
- **cpp built fails because ccd cmake was not found**: 
    - make sure you specified the paths to the roots of cr-rs and cr-dc correctly. 
    - Note that some compilers do not except the "-sign. 
    - Also note that the python version is specified WITHOUT the dot, so for example 39 for python 3.9.
- **cpp built fails because of boost template errors**: 
    - this usually occurs on apple or on some versions of GCC. Use GCC-10 to compile
- **cpp built fails for unknown reason**: 
    - follow the cpp build part like in the [README for dev](https://gitlab.lrz.de/cps/commonroad-reachable-set/-/blob/i4driving/README_FOR_DEVS.md) and use the flag `-vvv` for more verbose output. 
    - Check in the **CMakeList** of the respective project, how the path specifications for the repos (something like temp.manylinux.*:cython) are named and look into cr-dc and cr-rs build folders to see whether these files might have different names.
- **cpp built fails because libcrreach.a is not found**:
    - You probable did not follow the last part of step 6 in 1.2 Installation steps completely. Check the path in the error message and see if the `*.a` static library is where the CMakeList looks for it.
- **When running the python installation test scripts, the configuration path_root is not found**: 
    - cr-rs and cr-semantic expect as path root their respective root folder that contains both the folders `/configurations` and `/scenarios`. Theoretically, you can specify any path_root folder, as long as it contais the configurations and scenarios folders and valid configs and scenarios.
- **PyCharm cannot find commonroad_reach_semantic**: 
    - There seems to be a problem for submodules as root folder if you set the parent also as a root folder. Follow the installatin step 7 (in 1.2 Installation Steps)

## Usage
- **error: out of projection domain**: cr-dc uses curvi-linear coordinate frames (aka frenet-frames), which mathematically have a limited range where unique transformations to and from the cartesian frame are possible. If you exceed this boundaries when converting between curvi-linear and cartesian frame, this error is risen, since it is mathematically not possible, to provide a unique conversion. Either silence the error and ignore the lost information (reachable set is not transformable to cartesian anymore and is probably lost) or make sur in your code that you do not go outside the projection domain
