import numpy as np
import time
import copy

import cvxpy as cp

from collections import defaultdict
from data_structures.i4d_states import TargetState, GeneralState
from utils.cr_change_management.modify_config import temporarily_modify_config

# typing
from data_structures.agent_vehicle import AgentVehicle
from typing import List, Dict, Tuple






class DummyPlannerFollowReferencePathQP:
    """
    Dummy Planner that follows the reference path using QP min a².
    """

    def __init__(self, vehicle: AgentVehicle,
                 initial_state: GeneralState, target_state: TargetState):


        # vehicle
        self.vehicle: AgentVehicle = vehicle

        # dummy_params
        self.width = 1
        self.length = 1

        # optimization parameter
        self.dt: float = vehicle.config.planning.dt
        self.acc_lon_max: float = vehicle.config.vehicle.ego.a_lon_max
        self.acc_lon_min: float = vehicle.config.vehicle.ego.a_lon_min
        self.acc_lat_max: float = vehicle.config.vehicle.ego.a_lat_max
        self.acc_lat_min: float = vehicle.config.vehicle.ego.a_lat_min


        # states
        self.initial_state: GeneralState = initial_state
        self.target_state: TargetState = target_state

        # time
        self.t_start: int = initial_state.time_step
        self.t_end: int = target_state.time_step
        self.time_duration_index: int = self.t_end - self.t_start


        # solution in curvilinear
        self.trajectory_position_cvl: np.array = None
        self.trajectory_velocity_cvl: np.array = None
        self.trajectory_acceleration_cvl: np.array = None

        # target state dict
        self.dict_t_to_state = defaultdict()



    def generate_trajectory(self):
        """
        QP optimization based trajectory planning with minimum squared acceleration and reference path following.
        """

        # system matrix -> dim 4x4
        A = np.array([[1, self.dt, 0, 0],
                       [0, 1, 0, 0],
                       [0, 0, 1, self.dt],
                       [0, 0, 0, 1]], float)

        # input matrix -> dim: 4x4
        B = np.array([[self.dt ** 2 / 2, 0],
                       [self.dt, 0],
                       [0, self.dt ** 2 / 2],
                       [0, self.dt]], float)

        # state: (s, s_dot, d, d_dot) -> dim: 4x1 (or in cvxpy 4x0)
        initial_state = np.transpose(np.array([self.initial_state.centroid_cvl[0], self.initial_state.v_lon,
                                  self.initial_state.centroid_cvl[1], self.initial_state.v_lat], float))
        # TODO add initial reach node
        target_state = np.transpose(np.array([self.target_state.centroid_cvl[0], self.target_state.v_lon,
                                 self.target_state.centroid_cvl[1], self.target_state.v_lat], float))

        # time duration index
        f: int = self.time_duration_index

        # cost matrix –– will be squared in objective function
        Rsq = np.matrix('1, 0; 0, 2', float)

        # input (s_ddot, d_ddot)
        u = cp.Variable((2, f))

        # state
        x = cp.Variable((4, f + 1))

        constraints: List = [
            # initial state
            x[:, 0] == initial_state,

            # final state
            x[:, f] == target_state,

            # acceleration limits
            # longitudinal
            u[0, :] <= self.acc_lon_max * np.ones(f, float),
            self.acc_lon_min * np.ones(f, float) <= u[0, :],
            # lateral
            u[1, :] <= self.acc_lat_max * np.ones(f, float),
            self.acc_lat_min * np.ones(f, float) <= u[1, :],
        ]

        for k in range(0, f):
            constraints += [
                # dynamics
                x[:, k + 1] == A @ x[:, k] + B @ u[:, k],
            ]

        prob = cp.Problem(objective=cp.Minimize(cp.sum_squares(Rsq @ u)), constraints=constraints)

        # !WARNING! synchronization / numerical problems of cvxpy may occur if reachable set was calculated with
        # different vehicle parameters

        t_0 = time.perf_counter()
        result = prob.solve(cp.CVXOPT)
        print(f'cvxpy solving took {time.perf_counter() - t_0} s')


        if(prob.status != "optimal"):
            print("Status:")
            print(f'{prob.status}')
            print("trajectory data:")
            print(x.value)
            print("acceleration data:")
            print(u.value)
            raise ValueError(f'QP solver returned status {prob.status} instead of optimal. \n \
                             Check that the vehicle cvl acceleration constraints are the same that the ones in the optimization problem')



        # TODO implement suitable data format to store trajectory –– values already accessible (see below)
        state_vector_cvl = x.value
        acc_vector_cvl = u.value
        self.trajectory_position_cvl = np.asarray([state_vector_cvl[0, :], state_vector_cvl[2, :]], dtype=float)
        self.trajectory_velocity_cvl = np.asarray([state_vector_cvl[1, :], state_vector_cvl[3, :]], dtype=float)
        self.trajectory_acceleration_cvl = np.asarray(acc_vector_cvl, dtype=float)


        self._generate_tajectory_state_dict()



    def _generate_tajectory_state_dict(self):
        """
        Generates a trajectory state dict {time_step: State}
        """

        # TODO: Double check if the states still add up
        for idx_t in range(self.time_duration_index):
            t = self.t_start + idx_t
            if(idx_t == 0):
                # initial state
                a_lon = self.trajectory_acceleration_cvl[0, idx_t]
                a_lat = self.trajectory_acceleration_cvl[1, idx_t]
                self.initial_state.update_acceleration_from_cvl(a_lon, a_lat)
                self.dict_t_to_state[self.t_start] = self.initial_state
            else:
                # other states
                state = GeneralState(self.vehicle, t)
                p_lon = self.trajectory_position_cvl[0, idx_t]
                p_lat = self.trajectory_position_cvl[1, idx_t]
                v_lon = self.trajectory_velocity_cvl[0, idx_t]
                v_lat = self.trajectory_velocity_cvl[1, idx_t]
                a_lon = self.trajectory_acceleration_cvl[0, idx_t]
                a_lat = self.trajectory_acceleration_cvl[1, idx_t]
                state.init_state_from_curvilinear(p_lon, p_lat, v_lon, v_lat, a_lon, a_lat)
                self.dict_t_to_state[t] = state

        # end state
        self.dict_t_to_state[self.t_end] = self.target_state


    def get_trajectory_dict(self) -> Dict:
        """
        Return dict {time_step: GeneralState | InitialState}
        """
        return self.dict_t_to_state



    def get_state_at_step(self, step:int) -> TargetState | GeneralState:
        """
        Returns the state object instance of the step
        """
        if(step not in self.dict_t_to_state.keys()):
            raise ValueError(f'step {step} is not one of the states of the trajectory {self.dict_t_to_state.keys()}')

        return self.dict_t_to_state[step]