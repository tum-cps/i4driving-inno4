import logging
import copy
import numpy as np
from collections import defaultdict


from commonroad.scenario.scenario import Scenario

import commonroad_reach_semantic.data_structure.rule.priorities as priorities
from commonroad_reach_semantic.data_structure.environment_model.semantic_model import SemanticModel
from commonroad_reach_semantic.data_structure.rule.traffic_rule_interface import TrafficRuleInterface

from data_structures.configuration.scenario_configuration import ScenarioConfiguration

from data_structures.reach.collision_checker import ModifiedCollsionChecker
from data_structures.reach.semantic_reachable_set import ModifiedSemanticReachSet
from data_structures.reach.reachable_set_interface import ModifiedReachInterface





# typing
from typing import List, Optional
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from data_structures.target_trajectory import TargetTrajectory
    from commonroad_dc.pycrccosy import CurvilinearCoordinateSystem


logger = logging.getLogger(__name__)


class AgentVehicle:
    """
    Summary
    -------
    Class representing an agent vehicle.
    """
    cnt_id: int = 0
    dict_id_to_vehicle: dict = defaultdict()
    dict_id_planning_problem_to_vehicle: dict = defaultdict()
    vut_vehicle_id = None


    @classmethod
    def find_vehicle(cls,
                     id_vehicle: int = None,
                     id_planning_problem: int = None,
                     is_vut: bool = False) -> 'AgentVehicle':
        """
        Find vehicle by ONE (and only one) attribute, such as id_vehicle and id_planning_problem.
        """
        if(id_vehicle is not None):
            if(id_vehicle in cls.dict_id_to_vehicle.keys()):
                return cls.dict_id_to_vehicle[id_vehicle]
            else:
                print (f'vehicle with id {id_vehicle} not found')
                return None

        elif(id_planning_problem is not None):
            if (id_planning_problem in cls.dict_id_planning_problem_to_vehicle.keys()):
                return cls.dict_id_planning_problem_to_vehicle[id_planning_problem]
            else:
                print (f'vehicle with planning problem id {id_planning_problem} not found')
                return None

        elif(is_vut):
            if(cls.vut_vehicle_id is None):
                print(f'No vut vehicle found')
            return cls.dict_id_to_vehicle[cls.vut_vehicle_id]



    def __init__(self, config: ScenarioConfiguration):
        # Is target
        self.is_vut = False

        # Config
        self.config = config
        self.scenario: Scenario = config.scenario
        self.planning_problem = config.planning_problem

        # ids
        self.id = AgentVehicle.cnt_id
        self.dict_id_to_vehicle[self.id] = self
        AgentVehicle.cnt_id += 1

        # by planning problem id
        self.planning_problem_id = config.planning_problem.planning_problem_id
        self.dict_id_planning_problem_to_vehicle[config.planning_problem.planning_problem_id] = self


        # reach-interface
        self.reach_interface: Optional[ModifiedReachInterface] = None


        # Semantic Model
        self.semantic_model = SemanticModel(self.config)
        self.semantic_model.determine_traffic_priorities(priorities.dict_traffic_sign_to_priorities)
        self.rule_interface = TrafficRuleInterface(self.config, self.semantic_model)
        self.rule_interface.print_summary()

        # initialize reach interface
        self._initialize_reachable_set_interface()

        # overlapping
        self.dict_step_to_overlap = defaultdict(list)

        #  states
        self.dict_start_step_to_initial_state = defaultdict()
        self.dict_endstep_to_target_state = defaultdict()

        # trajectory
        self.dict_start_step_to_trajectory = defaultdict()

        # CLCS for visualization
        self.dict_step_to_clcs = defaultdict()

        # initial problem velocity
        self.initial_problem_velocity = copy.copy(config.planning_problem.initial_state.velocity)

    def _initialize_reachable_set_interface(self):
        print(f'Initializing vehicle {self.id}')
        self.reach_interface = ModifiedReachInterface(self.config)
        self.reach_interface._reach = ModifiedSemanticReachSet(self.config, self.semantic_model,
                                                                         self.rule_interface)
        self.reach_interface._reach.collision_checker = ModifiedCollsionChecker(self.config)


    def reset_and_update(self, t_start: int):
        """
        Reinitialize reachable set. Requires config to be changed first
        """
        previous_reachable_set = self.reach_interface.get_previous_multi_reachable_sets()

        self.semantic_model = SemanticModel(self.config)
        self.semantic_model.determine_traffic_priorities(priorities.dict_traffic_sign_to_priorities)
        self.rule_interface = TrafficRuleInterface(self.config, self.semantic_model)
        self.reach_interface = ModifiedReachInterface(self.config)
        self.reach_interface._reach = ModifiedSemanticReachSet(self.config, self.semantic_model,
                                                                         self.rule_interface)
        self.reach_interface._reach.collision_checker = ModifiedCollsionChecker(self.config)

        # Save previous reachable sets for later
        self.reach_interface.add_previous_multi_reachable_sets(previous_reachable_set)



    def compute_reachable_sets_for_time_steps(self, t_start: int, t_end: int):
        """
        Computes and propagates reachable set. Note that you have to initialize them first.
        """
        self.reach_interface.compute_drivable_areas_and_reachable_sets(t_start, t_end)




    def extract_driving_corridor(self, to_goal_region: bool = False) -> list:
        """
        Summary
        -------
        Extracts the driving corridor for the vehicle after negotiation

        Args
        ----
            to_goal_reagion (bool, optional): _description_. Defaults to False.
            debug (int, optional): _description_. Defaults to 0.

        Returns:
        -------
            list[DrivingCorridor]: driving corridor data for vehicle. A vehicle can have more than one feasible driving corridor.
        """

        longitudinal_driving_corridors = self.reach_interface.extract_driving_corridors(to_goal_region=to_goal_region)
        print(
            f'extracted {len(longitudinal_driving_corridors)} \
            possible driving corridors for the vehicle {self.id}')

        return longitudinal_driving_corridors



    # -------- Fake Getter and Setter -------------------------


    def set_as_vut(self, is_vut: bool = True) -> None:
        """
        Sets this vehicle as target.
        """
        self.is_vut = is_vut
        AgentVehicle.vut_vehicle_id = self.id

    def get_drivable_area_at_step(self, step: int) -> list:
        """
        Returns drivable area at step, which is a list of polygons.
        """
        return self.reach_interface.drivable_area_at_step(step)

    def get_reachable_set_at_step(self, step: int) -> list:
        """
        Returns reachable set at step, which is a list of ReachNode
        """
        return self.reach_interface.reachable_set_at_step(step)

    def set_overlaps_at_step(self, list_overlap_at_step: List, step: int) -> None:
        """
        Append list of overlap at step.
        """
        self.dict_step_to_overlap[step] = list_overlap_at_step

    def get_overlaps_at_step(self, step: int) -> list:
        """
        Returns a list of overlaps for that vehicle at step.
        """
        return self.dict_step_to_overlap[step]


    def set_target_state_for_step(self, target_state, end_step: int) -> None:
        """
        sets target state for end step
        """
        self.dict_endstep_to_target_state[end_step] = target_state


    def set_initial_state_for_step(self, initial_state, start_step: int) -> None:
        """
        Set initial state of vehicle
        """
        self.dict_start_step_to_initial_state[start_step] = initial_state

    def get_target_state_for_end_step(self, end_step: int):
        """
        Returns target_state for end step
        """
        if(end_step in self.dict_endstep_to_target_state.keys()):
            return self.dict_endstep_to_target_state[end_step]
        else:
            return None


    def set_trajectory_for_start_step(self, trajectory,
                                      start_step: int) -> None:
        """
        Sets trajectory for start step
        """
        self.dict_start_step_to_trajectory[start_step] = trajectory



    def get_trajectory_for_start_step(self, start_step: int) -> "TargetTrajectory":
        """
        Return trajectory object for start state
        """
        return self.dict_start_step_to_trajectory[start_step]



    def get_initial_position_cvl(self) -> np.array:
        """
        Get initial position in curvilinear coordinates
        """
        position_array_cvl = np.array([self.config.planning.p_lon_initial, self.config.planning.p_lat_initial])
        return position_array_cvl


    def get_initial_velocity_cvl(self) -> np.array:
        """
        Get initial velocity in curvilinear coordinates
        """
        velocity_array_cvl = np.array([self.config.planning.v_lon_initial, self.config.planning.v_lat_initial])
        return velocity_array_cvl


    def get_trajectory(self, t_start: int) -> "TargetTrajectory":
        return self.dict_start_step_to_trajectory[t_start]


    def set_clcs_at_initial_step(self, initial_step: int, CLCS: "CurvilinearCoordinateSystem") -> None:
        """
        Set curvilinear coordinate frame instance CLCS for initial step. Used for plotting later
        """
        self.dict_step_to_clcs[initial_step] = copy.deepcopy(CLCS)


    def get_clcs_at_step(self, step: int) -> "CurvilinearCoordinateSystem":
        """
        Get curvilinear coordinate frame instance CLCS for step.
        """
        if(step in self.dict_step_to_clcs.keys()):
            return self.dict_step_to_clcs[step]

        else:
            # Find closest step
            delta=1000
            closest_step = -1
            for s in self.dict_step_to_clcs.keys():
                if(s <= step and (step-s)<delta):
                    delta = (step-s)
                    closest_step = s

            return self.dict_step_to_clcs[closest_step]





