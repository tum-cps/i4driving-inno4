import copy
import math
from collections import defaultdict
import numpy as np


# Third party
from shapely import Polygon as ShapelyPolygon
from shapely import affinity


# Own Code Base
import utils.cr_change_management.curvilinear_cartesian as curv_cart
from utils.reach_nodes.reach_node_processing import (interpolate_velocity_by_cvl_position_and_orientation,
                                                     find_reach_node_with_highest_velocity_range,
                                                     find_reach_node_with_minimal_distance_to_point,
                                                     get_point_in_reach_node)
from utils.scenario_modification.state_generation import AgentInitialStateFactory


# typing
from data_structures.overlapping_region import OverlappingRegion
from commonroad.scenario.state import InitialState
from commonroad_reach.data_structure.reach.reach_node import ReachNode
from typing import List, Dict, Tuple
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from data_structures.agent_vehicle import AgentVehicle
    from data_structures.target_trajectory import TargetTrajectory
    from data_structures.i4d_states import TargetState, GeneralState


class CollisionFactory:
    """
    Factory for collision, returns Collision Object instance or None, if no collision
    """

    @classmethod
    def check_collision(cls, trajectory_a: "TargetTrajectory", trajectory_b: "TargetTrajectory",
                        t_start: int, t_end: int) -> Tuple[bool, List["Collision"]]:
        """
        Checks if the trajectories of two vehicles collide in a given time frame. Returns collision flag and list of collisions
        """
        # Return values
        collision_flag: bool = False
        list_collisions = list()
        for step in range(t_start, t_end + 1):
            state_a: "TargetState | GeneralState" = trajectory_a.get_state_at_step(step)
            state_b: "TargetState | GeneralState" = trajectory_b.get_state_at_step(step)

            # Check intersection via Polygon
            polygon_a_cart: ShapelyPolygon = state_a.generate_cartesian_shapely_polygon()
            polygon_b_cart: ShapelyPolygon = state_b.generate_cartesian_shapely_polygon()

            # If there is an intersection, save it as collision object
            if (polygon_a_cart.intersects(polygon_b_cart)):
                collision_flag = True
                collision = Collision(trajectory_a.vehicle, trajectory_b.vehicle, step, polygon_a_cart, polygon_b_cart)
                list_collisions.append(collision)

        return collision_flag, list_collisions



class Collision:
    """
    A collision object between two vehicles
    """

    id_counter: int = 0
    list_instances: list = list()


    @classmethod
    def find_collision_object(cls, vehicle: "AgentVehicle") -> list:
        """
        Find object instance via query params
        """
        query_list = list()
        for collision in cls.list_instances:
            if(collision.vehicle_a is vehicle or collision.vehicle_b is vehicle):
                query_list.append(collision)

        return query_list


    def __init__(self, vehicle_a: "AgentVehicle", vehicle_b: "AgentVehicle", t_collision: int,
                 polygon_a: ShapelyPolygon, polygon_b: ShapelyPolygon):
        # for tracking
        self.list_instances.append(self)
        self.id = self.id_counter
        self.id_counter += 1


        # Input params
        self.vehicle_a: "AgentVehicle" = vehicle_a
        self.vehicle_b: "AgentVehicle" = vehicle_b

        self.polygon_a: "ShapelyPolygon" = polygon_a
        self.polygon_b: "ShapelyPolygon" = polygon_b


        # Output
        self.t_collision: int = t_collision
        self.shapely_polygon_cart: ShapelyPolygon = polygon_a.intersection(polygon_b)
        self.centroid_cart: np.ndarray = np.asarray([self.shapely_polygon_cart.centroid.x,
                                                     self.shapely_polygon_cart.centroid.y])


        # Output
        self.t_collision: int = t_collision
        self.shapely_polygon: ShapelyPolygon = polygon_a.intersection(polygon_b)
        self.centroid_cart: np.ndarray = np.asarray([self.shapely_polygon.centroid.x, self.shapely_polygon.centroid.y])








