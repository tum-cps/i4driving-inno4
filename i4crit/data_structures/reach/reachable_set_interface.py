from commonroad_reach.data_structure.reach.reach_interface import ReachableSetInterface
from data_structures.configuration.scenario_configuration import ScenarioConfiguration

import time
from collections import defaultdict

from pathlib import Path


class ModifiedReachInterface(ReachableSetInterface):
    """
    Modified Version of the reachable set interface via inheritance
    """

    def __init__(self, config: ScenarioConfiguration):
        super().__init__(config)
        self.t_0 = None

        # Variables that save the reachable set for each time step.
        self.multi_iteration_reachable_set = defaultdict()


    def add_previous_multi_reachable_sets(self, reachable_set: dict):
        """
        Adds reachable sets from previous iterations.
        """
        for k, v in reachable_set.items():
            if (k not in self.multi_iteration_reachable_set.keys()):
                self.multi_iteration_reachable_set[k] = v


    def get_previous_multi_reachable_sets(self) -> dict:
        """
        Return dict for reachable sets
        """
        return self.multi_iteration_reachable_set


    def multi_iteration_reachable_set_at_step(self, step: int) -> dict:
        """
        Returns reachable set at step for multi iterations drawing
        """
        return self.multi_iteration_reachable_set[step]

    def update_initial_state(self, t_0: int):
        """
        Updates initial state of cr-reach backend
        """
        self.t_0 = t_0
        self._reach.update_initial_states(t_0)

    def update_end_state(self, t_end: int):
        """
        updated end step of cr_rech_backend
        """
        self._reach.update_end_state(t_end)


    def reset_interface(self):
        self._reachable_set_computed = False
        self._driving_corridor_extractor = None


    def compute_drivable_areas_and_reachable_sets(self, step_start: int = 0, step_end: int = 0):
        """
        Computes drivable areas and reachable sets between the given start and end steps using the compute method.
        """
        print("* Computing reachable sets...")

        if (not self._reach):
            print("Reachable set is not initialized, aborting computation.")
            return None

        step_start = step_start if step_start else self.step_start + 1
        step_end = step_end if step_end else self.step_end

        if not (0 < step_start <= step_end):
            print("Steps for computation are invalid, aborting computation.")
            return None

        time_start = time.time()
        self._reach.compute(step_start, step_end)
        self._reachable_set_computed = True
        time_computation = time.time() - time_start

        print(f"\tTook: \t{time_computation:.3f}s")

        # Save config to output folder
        if self.config.debug.save_config:
            path_output = self.config.general.path_output
            Path(path_output).mkdir(parents=True, exist_ok=True)
            self.config.save(path_output, str(self.config.scenario.scenario_id))
            print("\tConfiguration file saved.")



    def propagated_set_at_step(self, step: int):
        if (not self._reachable_set_computed and step != self.t_0):
            print("Reachable set is not computed, retrieving propagated set failed.")
            return []

        else:
            return self._reach.propagated_set_at_step(step)

    def drivable_area_at_step(self, step: int):
        if (not self._reachable_set_computed and step != self.t_0):
            print("Reachable set is not computed, retrieving drivable area failed.")
            return []

        else:
            return self._reach.drivable_area_at_step(step)

    def reset_drivable_area_at_step(self, step: int, drivable_area):
        if (not self._reachable_set_computed and step != self.t_0):
            print("Reachable set is not computed, resetting drivable area failed.")
            return []

        else:
            return self._reach.reset_drivable_area_at_step(step, drivable_area)

    def reachable_set_at_step(self, step: int):
        if (not self._reachable_set_computed and step != self.t_0):
            print("Reachable set is not computed, retrieving reachable set failed.")
            return []

        else:
            return self._reach.reachable_set_at_step(step)

    def reset_reachable_set_at_step(self, step: int, reachable_set):
        if (not self._reachable_set_computed and step != self.t_0):
            print("Reachable set is not computed, resetting reachable set failed.")
            return []

        else:
            return self._reach.reset_reachable_set_at_step(step, reachable_set)