# Parent
from commonroad_reach_semantic.data_structure.reach.semantic_splitting_otf_reach_set_py import PySemanticSplittingOTFReachableSet


# Typing
from commonroad_reach_semantic.data_structure.config.semantic_configuration import SemanticConfiguration
from commonroad_reach_semantic.data_structure.environment_model.semantic_model import SemanticModel
from commonroad_reach_semantic.data_structure.model_checking.finite_automaton import FiniteAutomaton

from commonroad_reach_semantic.data_structure.rule.traffic_rule_interface import TrafficRuleInterface




class ModifiedSemanticReachSet(PySemanticSplittingOTFReachableSet):

    def __init__(self, config: SemanticConfiguration, semantic_model: SemanticModel,
                 rule_interface: TrafficRuleInterface):
        super().__init__(config, semantic_model, rule_interface)


    def update_initial_states(self, t_start: int):
        """
        Changes Semantic Reach so that you choose the initial state arbitrarily
        """
        self._initialize_zero_state_polygons()

        # Construct finite automaton from traffic rules
        self.automaton = FiniteAutomaton(self.rule_interface.list_specifications_ltl, self.config.traffic_rule.mode_automata)

        # Compute initial reachable set
        self.compute_drivable_area_at_step(t_start)
        self.compute_reachable_set_at_step(t_start)

        print("PySemanticOTFReachableSet initialized.")


    def update_end_state(self, t_end: int):
        """
        Updates end state
        """
        self.step_end = t_end



