from abc import ABC

import warnings
import copy
import numpy as np

# third party
from shapely import Polygon as ShapelyPolygon
from shapely import affinity
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon as PlotPolygon

# commonroad
from commonroad_reach.utility import reach_operation
from commonroad_reach.data_structure.reach.reach_polygon import ReachPolygon


# own libs
from data_structures.agent_vehicle import AgentVehicle
import utils.cr_change_management.curvilinear_cartesian as curv_cart
from visualization.scenario_visualization import compute_plot_limits_from_road_network


# typing
from typing import List, Tuple, Dict
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from commonroad_reach.data_structure.reach.reach_polygon import ReachPolygon
    from data_structures.agent_vehicle import AgentVehicle
    from commonroad.scenario.scenario import Scenario


class FastReachPolygon:
    """
    More handy class for fast reach-polygon overapproximation
    """



    @classmethod
    def debug_plot_polygon(cls,
                           scenario: "Scenario",
                           latest_drivable_area_A: ShapelyPolygon,
                           latest_drivable_area_B: ShapelyPolygon) -> None:
        """
        plots the last computed reach polygons of two vehicles
        """

        plt_polygon_A = PlotPolygon(latest_drivable_area_A.exterior.coords)
        plt_polygon_B = PlotPolygon(latest_drivable_area_B.exterior.coords)

        fig, ax = plt.subplots()

        plot_limits = compute_plot_limits_from_road_network(scenario)
        ax.set_xlim([plot_limits[0], plot_limits[1]])
        ax.set_ylim([plot_limits[2], plot_limits[3]])

        ax.add_patch(plt_polygon_A)
        ax.add_patch(plt_polygon_B)

        plt.show()






    def __init__(self, vehicle: "AgentVehicle") -> None:
        self.vehicle: AgentVehicle = vehicle

        # Curvilinear polygon
        self.polygon_lon_init: ReachPolygon = None
        self.polygon_lat_init: ReachPolygon = None


        self.list_polygon_lon: List[ReachPolygon] = list()
        self.list_polygon_lat: List[ReachPolygon] = list()


        # drivable area in cartesian
        self.list_shapely_drivable_area_cart: List[ShapelyPolygon] = list()



    def init_zero_state_polygon(self):
        """
        Initializes zero state polygon and generates first step polygon.
        """
        self.polygon_lon_init: ReachPolygon = reach_operation.create_zero_state_polygon(self.vehicle.config.planning.dt,
                                                                            self.vehicle.config.vehicle.ego.a_lon_min,
                                                                            self.vehicle.config.vehicle.ego.a_lon_max)


        self.polygon_lat_init: ReachPolygon = reach_operation.create_zero_state_polygon(self.vehicle.config.planning.dt,
                                                                                self.vehicle.config.vehicle.ego.a_lat_min,
                                                                                self.vehicle.config.vehicle.ego.a_lat_max)



        #FIXME: Polygon init and first polygon are two different things. init_polygon is used every time for propagation
        p_lon_init: float = self.vehicle.config.planning.p_lon_initial
        p_lat_init: float = self.vehicle.config.planning.p_lat_initial
        v_lon_init: float = self.vehicle.config.planning.v_lon_initial
        v_lat_init: float = self.vehicle.config.planning.v_lat_initial


        first_polygon_lon: "ReachPolygon" = self._move_init_polygon_cvl_to_init_position_and_velocity(self.polygon_lon_init,
                                                                                      p_lon_init, v_lon_init)

        first_polygon_lat: "ReachPolygon" = self._move_init_polygon_cvl_to_init_position_and_velocity(self.polygon_lat_init,
                                                                                      p_lat_init, v_lat_init)


        self.list_polygon_lon.append(first_polygon_lon)
        self.list_polygon_lat.append(first_polygon_lat)

        # FIXME: initial state out of bound -> initial state tranlsation and rotation is missing
        self._generate_drivable_area_cartesian(first_polygon_lon, first_polygon_lat)



    def propagate_polygon_one_step(self):
        """
        Propagates polygon one time step further.
        """

        # propagate in both directions
        polygon_lon_propagated: "ReachPolygon" = reach_operation.propagate_polygon(self.list_polygon_lon[-1],
                                                                   self.polygon_lon_init,
                                                                   self.vehicle.config.planning.dt,
                                                                   self.vehicle.config.vehicle.ego.v_lon_min,
                                                                   self.vehicle.config.vehicle.ego.v_lon_max)

        polygon_lat_propagated: "ReachPolygon" = reach_operation.propagate_polygon(self.list_polygon_lat[-1],
                                                                   self.polygon_lat_init,
                                                                   self.vehicle.config.planning.dt,
                                                                   self.vehicle.config.vehicle.ego.v_lat_min,
                                                                   self.vehicle.config.vehicle.ego.v_lat_max)

        self.list_polygon_lon.append(polygon_lon_propagated)
        self.list_polygon_lat.append(polygon_lat_propagated)


        self._generate_drivable_area_cartesian(polygon_lon_propagated, polygon_lat_propagated)



    def _move_init_polygon_cvl_to_init_position_and_velocity(self, polygon: "ReachPolygon",
                                                             position_coord_init_cvl: float,
                                                             velocity_coord_init_cvl: float) -> "ReachPolygon":
        """
        Moves the initial state polygon in curvilinear to the initial position of the vehicle.
        """

        position_min_cvl: float = polygon.p_lon_min + position_coord_init_cvl
        position_max_cvl: float = polygon.p_lon_max + position_coord_init_cvl
        velocity_min: float = polygon.v_min + velocity_coord_init_cvl
        velocity_max: float = polygon.v_max + velocity_coord_init_cvl


        translated_polygon = ReachPolygon.from_rectangle_vertices(position_min_cvl, velocity_min,
                                                                  position_max_cvl, velocity_max)

        return translated_polygon



    def _generate_drivable_area_cartesian(self, polygon_lon: "ReachPolygon", polygon_lat: "ReachPolygon") -> None:
        """
        Generates a drivable area as shapely polygon in cartesian coordinates.
        """
        p_lon_min: float = polygon_lon.p_lon_min
        p_lon_max: float = polygon_lon.p_lon_max
        p_lat_min: float = polygon_lat.p_lat_min
        p_lat_max: float = polygon_lat.p_lat_max


        list_vertices = [(p_lon_min, p_lat_min), (p_lon_max, p_lat_min),
                         (p_lon_max, p_lat_max), (p_lon_min, p_lat_max)]

        shapely_polygon_curv = ShapelyPolygon(list_vertices)

        shapely_polygon_cart = curv_cart.convert_shapely_polygon_curvilinear_to_cartesian(self.vehicle, shapely_polygon_curv)

        self.list_shapely_drivable_area_cart.append(shapely_polygon_cart)




    def get_current_shapely_drivable_area_cart(self) -> ShapelyPolygon:
        """
        Return the last calcualted drivable area as shapely object
        """
        return self.list_shapely_drivable_area_cart[-1]








