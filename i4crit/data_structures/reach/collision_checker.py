from commonroad_reach.data_structure.collision_checker import CollisionChecker
from data_structures.configuration.scenario_configuration import ScenarioConfiguration

from commonroad_dc.boundary import boundary

# cpp libraries
import commonroad_reach.pycrreach as reach
import commonroad_dc.pycrcc as pycrcc

from typing import List
from commonroad.geometry.shape import Rectangle, ShapeGroup, Polygon, Circle
from commonroad.scenario.obstacle import DynamicObstacle


class ModifiedCollsionChecker(CollisionChecker):

    def __init(self, config: ScenarioConfiguration):
        super().__init__(config)


    # Overwrite the method so that virtual objects are ignored
    def obtain_vertices_of_polygons_for_dynamic_obstacles(self, list_obstacles_dynamic: List[DynamicObstacle],
                                                          consider_traffic: bool):
        """
        Returns the vertices of polygons from a given list of dynamic obstacles.
        """
        step_start = self.config.planning.step_start
        step_end = step_start + self.config.planning.steps_computation + 1

        dict_time_to_list_vertices_polygons_dynamic = {step: [] for step in range(step_start, step_end)}

        if consider_traffic:
            for step in range(step_start, step_end):
                list_vertices_polygons_dynamic = []
                time_step = step * round(self.config.planning.dt / self.config.scenario.dt)
                for obstacle in list_obstacles_dynamic:

                    # check if this is a virtual agent, if so, ignore it.
                    if(hasattr(obstacle, 'is_virtual_agent')):
                        continue

                    occupancy = obstacle.occupancy_at_time(time_step)
                    if not occupancy:
                        continue
                    shape = occupancy.shape

                    if isinstance(shape, Rectangle) or isinstance(shape, Polygon):
                        list_vertices_polygons_dynamic.append(shape.vertices)
                    elif isinstance(shape, Circle):
                        list_vertices_polygons_dynamic.append(Polygon(shape.shapely_object.buffer(0.01).exterior).vertices)
                    elif isinstance(shape, ShapeGroup):
                        for shape in shape.shapes:
                            list_vertices_polygons_dynamic.append(shape.vertices)
                    else:
                        raise NotImplementedError(f"collision_checker.py: "
                                                  f"obstacle occupancy shape {shape} is not supported!")

                dict_time_to_list_vertices_polygons_dynamic[step] += list_vertices_polygons_dynamic

        return dict_time_to_list_vertices_polygons_dynamic


    def _create_curvilinear_collision_checker(self) -> pycrcc.CollisionChecker:
        """
        Creates a curvilinear collision checker.

        The collision checker is created by adding a shape group containing occupancies of all static obstacles, and a
        time variant object containing shape groups of occupancies of all dynamic obstacles at different steps.
        """
        scenario = self.config.scenario
        lanelet_network = self.config.planning.lanelet_network

        # static obstacles
        list_obstacles_static = self.retrieve_static_obstacles(scenario, lanelet_network,
                                                               self.config.reachable_set.consider_traffic)
        list_vertices_polygons_static = self.obtain_vertices_of_polygons_from_static_obstacles(list_obstacles_static)

        # dynamic obstacles
        list_obstacles_dynamic = scenario.dynamic_obstacles
        dict_time_to_list_vertices_polygons_dynamic = \
            self.obtain_vertices_of_polygons_for_dynamic_obstacles(list_obstacles_dynamic,
                                                                   self.config.reachable_set.consider_traffic)

        # TODO: tmasc Quick and Dirty road boundaries
        print(f'creating road boundaries with axis alighned trianguluation as obstacles')
        object_road_boundary, _ = boundary.create_road_boundary_obstacle(scenario,  method='aligned_triangulation', axis=2)
        list_road_boundary_obstacles = self.obtain_vertices_of_polygons_from_static_obstacles([object_road_boundary])
        list_obstacles_static.extend(list_road_boundary_obstacles)

        return reach.create_curvilinear_collision_checker(list_vertices_polygons_static,
                                                          dict_time_to_list_vertices_polygons_dynamic,
                                                          self.config.planning.CLCS,
                                                          self.config.vehicle.ego.radius_inflation, 4,
                                                          self.config.reachable_set.rasterize_obstacles,
                                                          self.config.reachable_set.rasterize_exclude_static)