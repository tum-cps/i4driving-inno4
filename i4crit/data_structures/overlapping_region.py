from collections import defaultdict
from typing import List
from data_structures.agent_vehicle import AgentVehicle
from utils.cr_change_management.curvilinear_cartesian import convert_point_curvillinear_to_cartesian

from commonroad_reach.utility.reach_operation import create_repartitioned_rectangles

from shapely import intersection as shapely_intersection
from shapely import Polygon as ShapelyPolygon
import copy
import numpy as np

# Typing
from commonroad_reach.data_structure.reach.reach_node import ReachNode, ReachPolygon




class OverlappingRegion:
    """
    Class describing an overlapping region
    """

    region_id_cnt: int = 0
    dict_id_to_region = defaultdict()
    dict_vehicle_to_regions = defaultdict(list)


    @classmethod
    def find_overlapping_regions(cls,
                                 region_id: int = None,
                                 ego_vehicle: AgentVehicle = None,
                                 other_vehicle: AgentVehicle = None,
                                 step: int = None) -> List:

        """
        Find regions with 5 possibilites.
        """

        # find by id
        if(region_id is not None):
            return [cls.dict_id_to_region[region_id]]

        # find for ego_vehicle for all steps and all vehicles
        elif(ego_vehicle is not None and other_vehicle is None and step is None):
            return cls.dict_vehicle_to_regions[ego_vehicle]

        # find for ego_vehicle for all vehicles at step
        elif(ego_vehicle is not None and other_vehicle is None and step is not None):
            list_regions = list()
            for region in cls.dict_vehicle_to_regions[ego_vehicle]:
                if (region.step == step):
                    list_regions.append(region)
            return list_regions

        # find for ego_ehicle at for one other vehicle at all steps
        elif(ego_vehicle is not None and other_vehicle is not None and step is None):
            list_regions = list()
            for region in cls.dict_vehicle_to_regions[ego_vehicle]:
                if (region.other_vehicle == other_vehicle):
                    list_regions.append(region)
            return list_regions

        # find for ego_vehicel and one other vehicle at step
        elif(ego_vehicle is not None and other_vehicle is not None and step is not None):
            list_regions = list()
            for region in cls.dict_vehicle_to_regions[ego_vehicle]:
                if (region.other_vehicle == other_vehicle and region.step == step):
                    list_regions.append(region)
            return list_regions



    def __init__(self, ego_vehicle: AgentVehicle, other_vehicle: AgentVehicle,
                 list_reach_node_ego: List[ReachNode], list_reach_node_other: List[ReachNode],
                 intersection_polygon: ShapelyPolygon,
                 step: int):

        # set id
        self.id = OverlappingRegion.region_id_cnt
        OverlappingRegion.region_id_cnt += 1

        # properties
        self.ego_vehicle: AgentVehicle = ego_vehicle
        self.other_vehicle: AgentVehicle = other_vehicle
        self.intersection_polygon: ShapelyPolygon = intersection_polygon
        self.vertices = self._compute_vertices()
        self.area = intersection_polygon.area
        self.step = step

        # reachnodes
        self.list_reach_node_ego = list_reach_node_ego
        self.list_reach_node_other = list_reach_node_other

        # to find stuff later
        OverlappingRegion.dict_vehicle_to_regions[ego_vehicle].append(self)



    def _compute_vertices(self):
        """
        Computes vertices of the shapely shapel_polygon
        """
        list_x, list_y = self.intersection_polygon.exterior.coords.xy
        return [vertex for vertex in zip(list_x, list_y)]




class OverlappingRegionFactory:
    """
    Class that creates overlapping regions objects, which describes an overlapping region as shapely shapel_polygon.
    """

    # grid size for sweep-line repartitioning
    repartition_grid_size: float = 0.025

    @classmethod
    def create_overlapping_regions_at_step(cls,
                                           ego_vehicle: AgentVehicle, list_vehicles: List[AgentVehicle],
                                           step: int) -> List[OverlappingRegion]:

        """
        Creates a list of overlapping regions for the ego vehicle to all other vehicles in list_vehicle at step.
        """

        # return value
        list_overlapping_regions: List[OverlappingRegion] = list()

        # for ego vehicle find overlap to all other vehicles abd create OverlappingRegion instance
        for vehicle in list_vehicles:
            if(ego_vehicle is vehicle):
                continue

            # get reachable sets
            reachable_set_ego = ego_vehicle.get_reachable_set_at_step(step)
            reachable_set_other = vehicle.get_reachable_set_at_step(step)

            # repartition drivable area in their respective curvilinear coordinate frame
            list_repartitioned_drivable_area_ego: List[ReachPolygon] = \
                cls._merge_overlapping_drivable_areas(reachable_set_ego)
            list_repartitioned_drivable_area_other: List[ReachPolygon] = \
                cls._merge_overlapping_drivable_areas(reachable_set_other)

            # for each drivable area, find intersection
            for reach_polygon_ego in list_repartitioned_drivable_area_ego:
                for reach_polygon_other in list_repartitioned_drivable_area_other:
                    # use python shapely library to find intersection of repartitioned polygons in cartesian cosys
                    intersection_polygon = shapely_intersection(
                        cls._convert_shapely_polygon_to_cartesian(reach_polygon_ego.shapely_object, ego_vehicle),
                        cls._convert_shapely_polygon_to_cartesian(reach_polygon_other.shapely_object, vehicle))

                    # if intersection exists
                    if(intersection_polygon is not None):
                        if(intersection_polygon.area > 0.0 and intersection_polygon):
                            # create overlapping region instance
                            overlapping_region = OverlappingRegion(ego_vehicle, vehicle,
                                                                   reachable_set_ego, reachable_set_other,
                                                                   intersection_polygon, step)

                            list_overlapping_regions.append(overlapping_region)

        return list_overlapping_regions



    @classmethod
    def _convert_shapely_polygon_to_cartesian(cls, shapely_polygon: ShapelyPolygon, vehicle: AgentVehicle):
        """
        Converts shapely shapely_polygon to from curvilinear to cartesain
        """
        list_x, list_y = shapely_polygon.exterior.coords.xy
        list_vertices = [vertex for vertex in zip(list_x, list_y)]

        list_converted_vertices = list()
        for vertex in list_vertices:
            try:
                vertex_converted = convert_point_curvillinear_to_cartesian(vehicle, vertex[0], vertex[1])
            except Exception as e:
                print(f'[overlapping_region.py] conversion of shapely polygon from cvl to cart cosys failed \
                 with exception: {e}. \n vertices_cvl: {list_vertices}     ')
                return None
            list_converted_vertices.append(vertex_converted)

        # shapely uses minimum_rotated_rectangel property to tackel the hourglass vs. rectangle problem of
        # vertices order
        converted_polygon = ShapelyPolygon(list_converted_vertices)# .minimum_rotated_rectangle
        return converted_polygon


    @classmethod
    def _merge_overlapping_drivable_areas(cls, reachable_set) -> List[ReachPolygon]:
        """
        Merges the drivable area of multiple reach nodes, since in reach-semantic, the OTF-Splitting results in
        overlapping drivable areas.
        Uses a sweepline algorithm for cr-reach
        """

        if(len(reachable_set) == 0):
            return []
        elif(len(reachable_set) == 1):
            return [reachable_set[0].position_rectangle]

        else:
            list_reach_polygons: List[ReachPolygon] = list()
            for reach_node in reachable_set:
                list_reach_polygons.append(reach_node.position_rectangle)

            # sweep-line merge and repartition
            list_repartitioned_polygon_hulls: List[ReachPolygon] = create_repartitioned_rectangles(list_reach_polygons,
                                                                                              cls.repartition_grid_size)

            return list_repartitioned_polygon_hulls




