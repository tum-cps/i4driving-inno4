import time
import warnings
from collections import defaultdict
import numpy as np

# third party
import cvxpy as cp
from shapely.geometry import Point
import matplotlib.pyplot as plt

# Owm code base
from data_structures.i4d_states import TargetState, GeneralState

# typing
from data_structures.agent_vehicle import AgentVehicle
from typing import List, Dict, Tuple
from commonroad_reach.data_structure.reach.reach_node import ReachNode




def check_clockwise_convexity_of_polygon(vertices: List[np.ndarray]) -> bool:
    """
    Checks convexity of a polygon by analysing the vertices. Returns True if so.

    Algorithm
    ------------
    1. Compute the connecting vector v_c between the starting vertex_0 and the next vertex_1
    2. Compute the clock-wise normal vector v_n to that connecting vector v_c
    3. Compute the connecting vector v_d from the next vertex_3, vertex_4, ... to the original vertex_0
    4. compute the normalized cross product to analyse the angle between the normal vector v_n and the new
    connecting vector v_d. Since the normalized cross product between to angles equals the cosine of the angle between
    those vertices, a normalized cross product smaller than zero means that vertex_3, vertex_2, and vertex_1 do not form
    a convex polygon
    5. Repeat the process for the next vertex (vertex_1) as origin vertex in the next step and then so on.
    """

    # index i is origin vertex
    for i in range(len(vertices) - 1):
        vector_origin_vertex_to_next_vertex: np.ndarray = np.array(vertices[i + 1]) - np.array(vertices[i])
        clockwise_normal_vector: np.ndarray = (
            np.array([vector_origin_vertex_to_next_vertex[1], -vector_origin_vertex_to_next_vertex[0]]))

        # index j are all after-next vertices of the current origin vertex i
        for j in range(i + 2, len(vertices) - 1):
            vector_origin_vertex_to_after_next_vertex: np.ndarray = np.array(vertices[j]) - np.array(vertices[i])

            cosine_vector: float = np.cross(clockwise_normal_vector, vector_origin_vertex_to_after_next_vertex, axis=0)


            # TODO: I dont think that covers angles > 270 degrees
            # check if vector has not more than 90 degrees
            if(cosine_vector < 0):
                warnings.warn(f'Found clockwise cosine {cosine_vector}')
                return False

    return True



def is_cw_convex(vertices: List) -> bool:
    for i in range(0, len(vertices) - 1):
        vec_to_next = np.array(vertices[i + 1]) - np.array(vertices[i])
        rot_cw_90_deg = np.array([vec_to_next[1], -vec_to_next[0]])
        for j in range(i + 2, len(vertices) - 1):
            vec_to_orig = np.array(vertices[j]) - np.array(vertices[i])
            product = rot_cw_90_deg * vec_to_orig
            value = float(product[0] + product[1])
            if value < 0:
                return False

    return True


def debug_print_convexity(vertices: List[np.ndarray]) -> None:
    """

    Debug print polygon vertices. Call if convexity check fails.
    """
    fig, ax = plt.subplots()

    ax.set_xlim([-10, 10])
    ax.set_ylim([-10, 10])

    for vertex in vertices:
        ax.plot(vertex[0], vertex[1], 'x')

    plt.show()





class TargetTrajectory:
    """
    Class that  saves and generates the target trajectory for a vehicle.
    Needs initial state and target state as input --> see data_structures.i4d_states
    """

    cnt_id = 0
    dict_id_to_trajectory = defaultdict()

    @classmethod
    def find_target_trajectory_object(cls,
                                      id: int = None):
        """
        Finds trajectory object instance given the arguments.
        """
        return cls.dict_id_to_trajectory[id]


    def __init__(self, vehicle: AgentVehicle,
                 initial_state: GeneralState, target_state: TargetState, algorithm: str = "QP"):

        # unique class id
        self.id = self.cnt_id
        self.dict_id_to_trajectory[self.id] = self
        self.cnt_id += 1

        # used algorithm
        # TODO: tmasc use algorithm object instead
        self.algorithm = algorithm

        # vehicle
        self.vehicle: AgentVehicle = vehicle

        # optimization parameter
        self.dt: float = vehicle.config.planning.dt
        self.acc_lon_max: float = vehicle.config.vehicle.ego.a_lon_max
        self.acc_lon_min: float = vehicle.config.vehicle.ego.a_lon_min
        self.acc_lat_max: float = vehicle.config.vehicle.ego.a_lat_max
        self.acc_lat_min: float = vehicle.config.vehicle.ego.a_lat_min

        # reachable set --> be careful not to use a different copy for sync problems may arise
        # you can also use vehicle.set_reachable_set(step) etc..
        self.reachable_sets_of_vehicle: Dict[List[ReachNode]] = vehicle.reach_interface.reachable_set

        # states
        self.initial_state: GeneralState = initial_state
        self.target_state: TargetState = target_state

        # time
        self.t_start: int = initial_state.time_step
        self.t_end: int = target_state.time_step
        self.time_duration_index: int = self.t_end - self.t_start

        # solution in curvilinear
        self.trajectory_position_cvl: np.array = None
        self.trajectory_velocity_cvl: np.array = None
        self.trajectory_acceleration_cvl: np.array = None

        # target state dict
        self.dict_t_to_state = defaultdict()


    def generate_trajectory(self,
                            acc_lon_cost: float = 1, acc_lat_cost: float = 2,
                            p_lon_cost: float = 1, v_lon_cost:float = 1,
                            p_lat_cost: float = 2, v_lat_cost:float = 3,
                            v_lat_goal_close_zero:bool = True):
        """
        QP optimization based trajectory planning within specification-compliant reachable sets.
        Args are cost for longitudinal and lateral acceleration (input matrix u) and for the state.
        If v_lat_goal_close_zero is set to true, v_lat should be as small as possible to avoid
        extreme orientations in goal pose

        Note that the cost terms are squared.
        """

        # system matrix -> dim 4x4
        A = np.array([[1, self.dt, 0, 0],
                      [0, 1, 0, 0],
                      [0, 0, 1, self.dt],
                      [0, 0, 0, 1]], float)

        # input matrix -> dim: 4x4
        B = np.array([[self.dt ** 2 / 2, 0],
                      [self.dt, 0],
                      [0, self.dt ** 2 / 2],
                      [0, self.dt]], float)

        # state: (s, s_dot, d, d_dot) -> dim: 4x1 (or in cvxpy 4x0)
        initial_state = np.transpose(np.array([self.initial_state.centroid_cvl[0], self.initial_state.v_lon,
                                               self.initial_state.centroid_cvl[1], self.initial_state.v_lat], float))


        # if v_lat should be close to zero
        if(v_lat_goal_close_zero):
            v_lat_goal = 0.0
        else:
            v_lat_goal = self.target_state.v_lat

        target_state = np.transpose(np.array([self.target_state.centroid_cvl[0], self.target_state.v_lon,
                                              self.target_state.centroid_cvl[1], v_lat_goal], float))




        # time duration index
        f: int = self.time_duration_index

        # Cost constraint formulation
        # cost matrices –– will be squared in objective function
        acc_lon_lat_cost: str = str(acc_lon_cost)+', 0; 0, '+str(acc_lat_cost)
        state_lon_lat_cost:str = (str(p_lon_cost) + ', 0, 0, 0; 0, ' + str(v_lon_cost) + ', 0, 0; 0, 0, ' +
                                  str(p_lat_cost) + ' 0; 0, 0, 0, ' + str(v_lat_cost))



        R_runn_sq = np.matrix(acc_lon_lat_cost, float)  # lateral accelerations 4 times more expensive than longitudinal ones
        Q_term_sq = np.matrix(state_lon_lat_cost, float)  # set cost parameters for different state deviations at target state

        # input (s_ddot, d_ddot)
        u = cp.Variable((2, f))

        # state
        x = cp.Variable((4, f + 1))

        constraints: List = [
            # initial state
            x[:, 0] == initial_state,

            # final state --> use position
            # x[:, f] == target_state,  # now as slack

            # acceleration limits
            # longitudinal
            u[0, :] <= self.acc_lon_max * np.ones(f, float),
            self.acc_lon_min * np.ones(f, float) <= u[0, :],
            # lateral
            u[1, :] <= self.acc_lat_max * np.ones(f, float),
            self.acc_lat_min * np.ones(f, float) <= u[1, :],
        ]

        active_reachnode = self.target_state.reach_node_attacker
        reachnode_sequence: List = [active_reachnode]



        # Basic Idea: Go backwards from goal-node and chose largest-area reach node at each level of the reachability
        # graph until you reach start node:
        # this is necessary since cr-reach-semantic currently has overlapping drivable areas due to semantic splitting
        while len(active_reachnode.list_nodes_parent) > 0:
            # choose largest "drivable area"  # TODO investigate whether other metrics might be better
            active_reachnode = max(active_reachnode.list_nodes_parent, key=lambda rn: rn.position_rectangle.area)
            reachnode_sequence.insert(0, active_reachnode)  # LiFo list

        # sanity checks
        assert len(reachnode_sequence) == f + 1
        assert reachnode_sequence[0].polygon_lon.shapely_object.contains(Point(initial_state[0:2]))
        assert reachnode_sequence[0].polygon_lat.shapely_object.contains(Point(initial_state[2:4]))


        for k in range(0, f):
            constraints += [
                # dynamics
            x[:, k + 1] == A @ x[:, k] + B @ u[:, k],
            ]


            # reachable sets
            #if not reachnode_sequence[k].step == k + self.initial_state.time_step:
                #raise ValueError(k, self.initial_state.time_step, reachnode_sequence[k].step)

            reachnode = reachnode_sequence[k]

            # longitudinal
            vertices_lon_np = np.array(reachnode.polygon_lon.convex_hull.exterior.coords.xy)

            # FIXME: FloFli this someties
            #if not is_cw_convex(list(np.transpose(vertices_lon_np))):
                #debug_print_convexity(list(np.transpose(vertices_lon_np)))
                #raise ValueError(f'vertices of longitudinal reach polygon not convex {list(np.transpose(vertices_lon_np))}')

            for i in range(0, vertices_lon_np.shape[1] - 2):
                vec_to_next = vertices_lon_np[:, i + 1] - vertices_lon_np[:, i]
                rot_90_deg = np.array([vec_to_next[1], -vec_to_next[0]])
                constraints += [
                    sum((x[0:2, k] - vertices_lon_np[:, i]) @ rot_90_deg) >= 0,
                ]

            # lateral
            vertices_lat_np = np.array(reachnode.polygon_lat.convex_hull.exterior.coords.xy)
            #if not is_cw_convex(list(np.transpose(vertices_lat_np))):
                #debug_print_convexity(list(np.transpose(vertices_lat_np)))
                #raise ValueError(f'vertices of lateral reach polygon not convex {list(np.transpose(vertices_lat_np))}')


            for i in range(0, vertices_lat_np.shape[1] - 2):
                vec_to_next = vertices_lat_np[:, i + 1] - vertices_lat_np[:, i]
                rot_90_deg = np.array([vec_to_next[1], -vec_to_next[0]])
                constraints += [
                    sum((x[2:4, k] - vertices_lat_np[:, i]) @ rot_90_deg) >= 0,
                ]

        prob = cp.Problem(objective=cp.Minimize(cp.sum_squares(R_runn_sq @ u) + 100 * cp.sum_squares(Q_term_sq @ (x[:, f] - target_state))), constraints=constraints)

        # !WARNING! synchronization / numerical problems of cvxpy may occur if reachable set was calculated with
        # different vehicle parameters

        t_0 = time.perf_counter()
        result = prob.solve(cp.CVXOPT, verbose=False)
        print(f'cvxpy solving took {time.perf_counter() - t_0} s')
        # print(f'target state was: {target_state}')
        # print(f'actual state is: {x.value[:,f]}')
        print(f'target state deviation {x.value[:, f] - target_state}')

        if prob.status != "optimal":
            print("Status:")
            print(f'{prob.status}')
            print("trajectory data:")
            print(x.value)
            print("acceleration data:")
            print(u.value)
            raise ValueError(f'QP solver returned status {prob.status} instead of optimal. \n \
                             Check that the vehicle cvl acceleration constraints are the same that the ones in the optimization problem')

        # TODO implement suitable data format to store trajectory –– values already accessible (see below)
        state_vector_cvl = x.value
        acc_vector_cvl = u.value
        self.trajectory_position_cvl = np.asarray([state_vector_cvl[0, :], state_vector_cvl[2, :]], dtype=float)
        self.trajectory_velocity_cvl = np.asarray([state_vector_cvl[1, :], state_vector_cvl[3, :]], dtype=float)
        self.trajectory_acceleration_cvl = np.asarray(acc_vector_cvl, dtype=float)

        self._generate_trajectory_state_dict()


    def _generate_trajectory_state_dict(self):
        """
        Generates a trajectory state dict {time_step: State}
        """

        # TODO: Double check if the states still add up
        for idx_t in range(self.time_duration_index + 1):
            t = self.t_start + idx_t
            if (idx_t == 0):
                # initial state
                a_lon = self.trajectory_acceleration_cvl[0, idx_t]
                a_lat = self.trajectory_acceleration_cvl[1, idx_t]
                self.initial_state.update_acceleration_from_cvl(a_lon, a_lat)
                self.dict_t_to_state[self.t_start] = self.initial_state
            elif(t < self.t_end):
                # other states
                state = GeneralState(self.vehicle, t)
                p_lon = self.trajectory_position_cvl[0, idx_t]
                p_lat = self.trajectory_position_cvl[1, idx_t]
                v_lon = self.trajectory_velocity_cvl[0, idx_t]
                v_lat = self.trajectory_velocity_cvl[1, idx_t]
                a_lon = self.trajectory_acceleration_cvl[0, idx_t]
                a_lat = self.trajectory_acceleration_cvl[1, idx_t]
                state.init_state_from_curvilinear(p_lon, p_lat, v_lon, v_lat, a_lon, a_lat)
                self.dict_t_to_state[t] = state

            else:
                # end state -> since we use relaxation, target and actually end state might differ slightly
                p_lon = self.trajectory_position_cvl[0, idx_t]
                p_lat = self.trajectory_position_cvl[1, idx_t]
                v_lon = self.trajectory_velocity_cvl[0, idx_t]
                v_lat = self.trajectory_velocity_cvl[1, idx_t]
                self.target_state.update_state_from_cvl(p_lon, p_lat, v_lon, v_lat)
                self.dict_t_to_state[self.t_end] = self.target_state

    def get_trajectory_dict(self) -> Dict:
        """
        Return dict {time_step: GeneralState | InitialState}
        """
        return self.dict_t_to_state


    def get_state_at_step(self, step:int) -> TargetState | GeneralState:
        """
        Returns the state object instance of the step
        """
        if(step not in self.dict_t_to_state.keys()):
            raise ValueError(f'step {step} is not one of the states of the trajectory {self.dict_t_to_state.keys()}')

        return self.dict_t_to_state[step]


