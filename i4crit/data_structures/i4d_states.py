import copy
import math
from collections import defaultdict
import numpy as np


# Third party
from shapely import Polygon as ShapelyPolygon
from shapely import affinity


# Own Code Base
import utils.cr_change_management.curvilinear_cartesian as curv_cart
from utils.reach_nodes.reach_node_processing import (interpolate_velocity_by_cvl_position_and_orientation,
                                                     find_reach_node_with_highest_velocity_range,
                                                     find_reach_node_with_minimal_distance_to_point,
                                                     get_point_in_reach_node)
from utils.scenario_modification.state_generation import AgentInitialStateFactory


# typing
from data_structures.overlapping_region import OverlappingRegion
from data_structures.agent_vehicle import AgentVehicle
from typing import List, Tuple
from commonroad.scenario.state import InitialState
from commonroad_reach.data_structure.reach.reach_node import ReachNode





class I4dState:
    """
    Class that saves on target state of a vehicle both in global cartesian and indiviual curvilinear frame.
    TODO: Maybe ABC??
    """

    cnt_id = 0
    dict_id_to_states = defaultdict()

    def __init__(self, vehicle: AgentVehicle, time_step: int):

        # id
        self.id: int = self.cnt_id
        self.cnt_id += 1
        self.dict_id_to_states[self.id] = self

        # step assigned to the TargetState
        self.time_step: int = time_step
        self.dt: float = vehicle.config.planning.dt

        # vehicle it belongs to
        self.vehicle: AgentVehicle = vehicle

        # reference path in cartesian
        self.reference_path: List[np.ndarray] = curv_cart.get_reference_path_of_vehicle_in_cartesian(self.vehicle)

        # centroid
        self.centroid = None

        # cartesian
        self.centroid_cart: np.array = None
        self.v_x: float = None
        self.v_y: float = None
        self.a_x: float = None
        self.a_y: float = None
        self.orientation_rad_cart: float = None

        # curvilinear
        self.centroid_cvl: np.array = None
        self.v_lon: float = None
        self.v_lat: float = None
        self.a_lon: float = None
        self.a_lat: float = None
        self.orientation_rad_cvl: float = None




    def generate_cartesian_shapely_polygon(self) -> ShapelyPolygon:
        """
        Generates a shapely polygon of the current state.
        """

        length: float = self.vehicle.config.vehicle.ego.length
        width: float = self.vehicle.config.vehicle.ego.width

        x_min: float = self.centroid_cart[0] - length/2
        x_max: float = self.centroid_cart[0] + length/2
        y_min: float = self.centroid_cart[1] - width/2
        y_max: float = self.centroid_cart[1] + width/2


        # shapely counter clockwise with start=end vertex

        vertex_0 = vertex_4 = (x_min, y_min)
        vertex_1 = (x_max, y_min)
        vertex_2 = (x_max, y_max)
        vertex_3 = (x_min, y_max)

        shapely_polygon = ShapelyPolygon([vertex_0, vertex_1, vertex_2, vertex_3, vertex_4])
        shapely_polygon: ShapelyPolygon = affinity.rotate(shapely_polygon, self.orientation_rad_cart, use_radians=True)

        return shapely_polygon






class GeneralState(I4dState):
    """
    Class that represents a general state of the vehicle, including initial state
    """

    def __init__(self, vehicle: AgentVehicle, time_step: int):
        super().__init__(vehicle, time_step)


    def init_state_from_curvilinear(self, p_lon: float, p_lat: float,
                                    v_lon: float, v_lat: float, a_lon: float=None, a_lat: float=None):
        """
        Initialize state with curvilinear variables
        """

        # curvilinear
        self.centroid_cvl = np.array([p_lon, p_lat])
        self.v_lon = v_lon
        self.v_lat = v_lat

        # cartesian
        self.centroid_cart = curv_cart.convert_point_curvillinear_to_cartesian(self.vehicle, p_lon, p_lat)
        velocity_cart = curv_cart.convert_velocity_vector_curvilinear_to_cartesian(self.vehicle, p_lon, v_lon, v_lat)
        self.v_x = velocity_cart[0]
        self.v_y = velocity_cart[1]

        # Orientation
        self.orientation_rad_cvl: float = math.atan2(self.v_lat, self.v_lon)
        self.orientation_rad_cart: float = math.atan2(self.v_y, self.v_x)

        # if acceleration is set, convert it
        if(a_lon is not None and a_lat is not None):
            self.a_lon = a_lon
            self.a_lat = a_lat
            acceleration_cart = curv_cart.convert_accelereration_curvilinear_to_cartesian(self.vehicle, p_lon, a_lon, a_lat)
            self.a_x = acceleration_cart[0]
            self.a_y = acceleration_cart[1]


    def init_state_from_cartesian(self, x: float, y: float, v_x: float, v_y: float, a_x: float=None, a_y: float=None):
        """
        Initialize state with cartesian variables.
        """

        # cartesian
        self.centroid_cart = np.asarray([x, y])
        self.v_x = v_x
        self.v_y = v_y


        # curvilinear
        self.centroid_cvl = curv_cart.convert_point_cartesian_to_curvilinear(self.vehicle, x, y)
        velocity_cvl = curv_cart.convert_velocity_vector_cartesian_to_curvilinear(self.vehicle, self.centroid_cart[0],
                                                                                  self.centroid_cart[1], v_x, v_y)
        self.v_lon = velocity_cvl[0]
        self.v_lat = velocity_cvl[1]


        # if acceleration is set, convert it
        if(a_x is not None and a_y is not None):
            self.a_x = a_x
            self.a_y = a_y
            acceleration_cart = curv_cart.convert_accelleration_cartesian_to_curvilinear(self.vehicle,
                                                                                         x, y,
                                                                                         a_x, a_y)
            self.a_lon = acceleration_cart[0]
            self.a_lat = acceleration_cart[1]

        # Orientation
        self.orientation_rad_cart: float = math.atan2(self.v_y, self.v_x)
        self.orientation_rad_cvl: float = math.atan2(self.v_lat, self.v_lon)




    def update_acceleration_from_cvl(self, a_lon: float, a_lat: float):
        """
        Update acceleration from cvl values.
        """
        self.a_lon = a_lon
        self.a_lat = a_lat
        acceleration_cart = curv_cart.convert_accelereration_curvilinear_to_cartesian(self.vehicle, self.centroid_cvl[0],
                                                                                      a_lon, a_lat)
        self.a_x = acceleration_cart[0]
        self.a_y = acceleration_cart[1]









class TargetState(I4dState):
    """
    Class that saves on target state of a vehicle both in global cartesian and indiviual curvilinear frame.
    """
    def __init__(self, overlapping_region: OverlappingRegion, vehicle: AgentVehicle, time_step: int):
        super().__init__(vehicle, time_step)

        # overlapping region
        self.overlapping_region: OverlappingRegion = overlapping_region

        # area
        self.area: float = None

        # reach node of the attacking (not VUT) vehicle
        self.reach_node_attacker: ReachNode = None




    def update_state_from_cvl(self, p_lon:float, p_lat:float, v_lon:float, v_lat:float) -> None:
        """
        Updates target state from curvilinear. Necessary to call after Planner has finished
        """
        self.centroid_cvl = np.asarray([p_lon, p_lat])
        self.v_lon = v_lon
        self.v_lat = v_lat


        # update cartesian
        self.centroid_cart = curv_cart.convert_point_curvillinear_to_cartesian(self.vehicle, p_lon, p_lat)
        velocity_cart = curv_cart.convert_velocity_vector_curvilinear_to_cartesian(self.vehicle, p_lon, v_lon, v_lat)

        self.v_x = velocity_cart[0]
        self.v_y = velocity_cart[1]

        # Orientation
        self.orientation_rad_cvl: float = math.atan2(self.v_lat, self.v_lon)
        self.orientation_rad_cart: float = math.atan2(self.v_y, self.v_x)



    def _compute_centroids(self) -> None:
        """
        Takes overlapping region in cartesian coords and returns centroid in cvl as np.array.
        """
        self.centroid = self.overlapping_region.intersection_polygon.centroid
        self.centroid_cart = np.array([self.centroid.x, self.centroid.y])
        self.centroid_cvl = curv_cart.convert_point_cartesian_to_curvilinear(self.vehicle,
                                                                        self.centroid_cart[0], self.centroid_cart[1])



    def derive_target_velocity_cvl(self) -> np.array:
        """
        Derive target velocity by slicing both the lateral and the longitudinal reach node and interpolate velocities.
        """

        # Compute centroid
        self._compute_centroids()

        # temporarily convert centroid position back to curvilinear

        # Use ego reachnode to interpolate velocity
        # longitudinal
        centroid_cvl_lon = self.centroid_cvl[0]


        # find reach node that contains the centroid with highest velocity range
        list_reach_node = self.overlapping_region.list_reach_node_other
        self.reach_node_attacker = find_reach_node_with_highest_velocity_range(self.centroid_cvl, list_reach_node)
        self.v_lon, self.v_lat = interpolate_velocity_by_cvl_position_and_orientation(self.centroid_cvl,
                                                                                      self.reach_node_attacker,
                                                                                      v_lat_zero=True)

        # cartesian
        vel_cart = curv_cart.convert_velocity_vector_curvilinear_to_cartesian(self.vehicle, centroid_cvl_lon,
                                                                              self.v_lon, self.v_lat)
        self.v_x: float = vel_cart[0]
        self.v_y: float = vel_cart[1]

        # Orientation
        self.orientation_rad_cvl: float = math.atan2(self.v_lat, self.v_lon)
        self.orientation_rad_cart: float = math.atan2(self.v_y, self.v_x)



    def heuristic_target_state_generation(self, t_start: int,  t_end: int) -> None:
        """
        Generates target state via heuristic, if no overlap is detected to VUT.
        Current heuristic is:
        1. assuming constant velocity to generate a temporary target point.
        2. find closest reachnode
        3. take its middle and the average velocity there
        """

        # Algorithm
        # --------------------------------------
        # find reachable set that is closest to const-velocity propagation
        # take middle of that reach set as posision
        # take average of split reachable set at position

        # convert initial position to curvilinear
        x_init, y_init = self.vehicle.planning_problem.initial_state.position

        # There might also be a problem with dt etc, because car gets slower
        try:
            p_lon_init, p_lat_init = curv_cart.convert_point_cartesian_to_curvilinear(self.vehicle, x_init, y_init)
        except:
            # temporarily revert CLCS
            new_CLCS = copy.deepcopy(self.vehicle.config.planning.CLCS)
            self.vehicle.config.planning.CLCS = self.vehicle.get_clcs_at_step(0)
            p_lon_init, p_lat_init = curv_cart.convert_point_cartesian_to_curvilinear(self.vehicle, x_init, y_init)
            self.vehicle.config.planning.CLCS = new_CLCS


        # Assumption: initial velocity is entirely in v_lon (i.e. the vehicle does not steer at t_start)
        dt: float = self.vehicle.config.planning.dt
        v_lon_initial: float = self.vehicle.initial_problem_velocity

        # Propagation. Note that p_lat_0 might require infeasible controller input
        p_lon_propagated: float = v_lon_initial * (t_end - t_start) * dt + p_lon_init
        p_lat_propagated: float = 0


        # find closest reach node
        temp_centroid_cvl = np.asarray([p_lon_propagated, p_lat_propagated])
        closest_reach_node = find_reach_node_with_minimal_distance_to_point(temp_centroid_cvl,
                                                                self.vehicle.reach_interface.reachable_set_at_step(t_end))

        self.reach_node_attacker = closest_reach_node

        
        # position
        self.centroid_cvl: np.array = get_point_in_reach_node(closest_reach_node,
                                                              temp_centroid_cvl[0], temp_centroid_cvl[1])


        # FIXME: Constant velocty might result into point outside projectino domain
        x, y = curv_cart.convert_point_curvillinear_to_cartesian(self.vehicle, self.centroid_cvl[0], self.centroid_cvl[1])
        self.centroid_cart: np.array = np.asarray([x,y])

        # velocity
        # longitudinal
        self.v_lon, self.v_lat = interpolate_velocity_by_cvl_position_and_orientation(self.centroid_cvl,
                                                                                      closest_reach_node,
                                                                                      v_lat_zero=True)

        # cartesian
        vel_cart = curv_cart.convert_velocity_vector_curvilinear_to_cartesian(self.vehicle, self.centroid_cvl[0],
                                                                              self.v_lon, self.v_lat)

        self.v_x = vel_cart[0]
        self.v_y = vel_cart[1]

        # Orientation
        self.orientation_rad_cvl: float = math.atan2(self.v_lat, self.v_lon)
        self.orientation_rad_cart: float = math.atan2(self.v_y, self.v_x)





    def vut_state_generation(self, t_start: int, t_end: int) -> None:
        """
        Generates target state via heuristic, if no overlap is detected to VUT.
        Follow reference path via kdtree search with constant velocity
        """

        # Algorithm
        # --------------------------------------
        # find reachable set that is closest to const-velocity propagation
        # take middle of that reach set as posision
        # take average of split reachable set at position

        # convert initial position to curvilinear
        x_init, y_init = self.vehicle.planning_problem.initial_state.position
        p_lon_init, p_lat_init = curv_cart.convert_point_cartesian_to_curvilinear(self.vehicle, x_init, y_init)

        # Assumption: initial velocity is entirely in v_lon (i.e. the vehicle does not steer at t_start)
        dt: float = self.vehicle.config.planning.dt
        v_lon_initial: float = self.vehicle.initial_problem_velocity

        # Propagation. Note that p_lat_0 might require infeasible controller input
        p_lon_propagated: float = v_lon_initial * (t_end - t_start) * dt + p_lon_init
        p_lat_propagated: float = 0



        # find closest reach node
        temp_centroid_cvl = np.asarray([p_lon_propagated, p_lat_propagated])
        closest_reach_node = find_reach_node_with_minimal_distance_to_point(temp_centroid_cvl,
                                                                            self.vehicle.reach_interface.reachable_set_at_step(
                                                                                t_end))

        self.reach_node_attacker = closest_reach_node

        # position
        self.centroid_cvl: np.array = get_point_in_reach_node(closest_reach_node,
                                                              temp_centroid_cvl[0], temp_centroid_cvl[1])


        try:
            x, y = curv_cart.convert_point_curvillinear_to_cartesian(self.vehicle,
                                                                     self.centroid_cvl[0],
                                                                     self.centroid_cvl[1])
        except:
            # temporarily revert CLCS
            # FIXME: If the first planning cycle already fails in conversion, because the projection domains
            # are not compatible, there is no initial CLCS here. So maby save it earlier in the scenario manager

            new_CLCS = copy.deepcopy(self.vehicle.config.planning.CLCS)
            self.vehicle.config.planning.CLCS = self.vehicle.get_clcs_at_step(0)
            x, y = curv_cart.convert_point_curvillinear_to_cartesian(self.vehicle, x_init, y_init)
            self.vehicle.config.planning.CLCS = new_CLCS


        self.centroid_cart: np.array = np.asarray([x, y])

        # velocity
        # longitudinal
        self.v_lon, self.v_lat = interpolate_velocity_by_cvl_position_and_orientation(self.centroid_cvl,
                                                                                      closest_reach_node)

        # cartesian
        vel_cart = curv_cart.convert_velocity_vector_curvilinear_to_cartesian(self.vehicle, self.centroid_cvl[0],
                                                                              self.v_lon, self.v_lat)

        self.v_x = vel_cart[0]
        self.v_y = vel_cart[1]


        # Orientation
        self.orientation_rad_cvl: float = math.atan2(self.v_lat, self.v_lon)
        self.orientation_rad_cart: float = math.atan2(self.v_y, self.v_x)



    def convert_to_cr_initial_state(self) -> InitialState:
        """
        Converts initial state to commonroad state instance
        """
        cr_state_cartesian: InitialState = AgentInitialStateFactory.create_initial_state_from_cartesian(self.centroid_cart,
                                                                                        self.v_x, self.v_y,
                                                                                    0, 0,
                                                                                        self.time_step,
                                                                                        self.vehicle)

        return cr_state_cartesian









