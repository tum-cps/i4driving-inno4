import warnings
from collections import defaultdict
import numpy as np



# Third Party

# commonroad

# Own Code Base
from data_structures.agent_vehicle import AgentVehicle
from data_structures.overlapping_region import OverlappingRegionFactory, OverlappingRegion
from data_structures.i4d_states import TargetState, GeneralState
from data_structures.target_trajectory import TargetTrajectory
from utils.cr_change_management.modify_config import temporarily_modify_config
from data_structures.configuration.configuration_builder import ConfigurationBuilder
from vehicle.vehicle_under_test.vut_qp_planner import DummyPlannerFollowReferencePathQP as DummyQP
from data_structures.collision import CollisionFactory



# typing
from typing import List, Tuple
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    pass



class ScenarioManager:
    """
    Manages the scenario, its propagation etc.
    """

    def __init__(self, list_vehicles: List[AgentVehicle]):
        """
        Takes a list of agent vehicles.
        """
        self.list_vehicles = list_vehicles
        self.dict_step_to_overlaps = defaultdict(list)
        self.dict_step_to_collisions = defaultdict(list)

    def perform_reachability_analysis(self, t_start, t_end, reinitialize=True):
        """
        Propagates reachable sets for all agent vehicles from t_start to t_end.
        If reinitialize, previous reachable set computations are discarded.
        """
        for vehicle in self.list_vehicles:
            old_config = temporarily_modify_config(vehicle, t_start, t_end)
            vehicle.reset_and_update(t_start)
            vehicle.compute_reachable_sets_for_time_steps(t_start, t_end)

            # Reset config and _reach
            # revert_modified_steps(vehicle, old_config)


    def find_overlap_in_reachable_sets(self, step: int):
        """
        Finds overlaps in reachable sets and saves them in vehicle class.
        """

        # for each vehicle, find overlap to all other vehicles at step
        for ego_vehicle in self.list_vehicles:
            list_overlapping_regions: List[OverlappingRegion] = OverlappingRegionFactory.create_overlapping_regions_at_step(
                                                                        ego_vehicle=ego_vehicle,
                                                                        list_vehicles=self.list_vehicles,
                                                                        step=step)

            ego_vehicle.set_overlaps_at_step(list_overlapping_regions, step)

            # Also save them in the scenario_manager
            self.dict_step_to_overlaps[step].append(list_overlapping_regions)

            x = 3


    def perform_planning(self, t_start: int, t_end: int, vut_vehicle: AgentVehicle):
        """
        Propagates planning. Needs an initial state at t_start, furthermore the conflicted regions should be members
        of the vehicle instance.
        """


        # determine target state for each vehicle (but the VUT)
        target_states = defaultdict(TargetState)
        for vehicle in self.list_vehicles:
            if (vehicle is not vut_vehicle):
                # get initial state object
                init_position_cvl = vehicle.get_initial_position_cvl()
                init_vel_cvl = vehicle.get_initial_velocity_cvl()
                initial_state = GeneralState(vehicle, t_start)
                initial_state.init_state_from_curvilinear(p_lon=init_position_cvl[0], p_lat=init_position_cvl[1],
                                                          v_lon=init_vel_cvl[0], v_lat=init_vel_cvl[1])

                # get target state
                target_state: TargetState = self._derive_target_state(t_start, t_end, vehicle.id, vut_vehicle)
                target_states[vehicle.id] = target_state

                # save target state in vehicle
                vehicle.set_initial_state_for_step(initial_state, t_start)
                vehicle.set_target_state_for_step(target_state, t_end)

                # generate trajectory
                trajectory = TargetTrajectory(vehicle, initial_state, target_state)
                trajectory.generate_trajectory()

                # save trajectory in vehicle
                vehicle.set_trajectory_for_start_step(trajectory, t_start)

                # generate dynamic obstacles from attackers and update planning problem
                vehicle.set_clcs_at_initial_step(t_start, vehicle.config.planning.CLCS)

                ConfigurationBuilder.add_attacking_vehicle_as_config_object(vut_vehicle, vehicle, trajectory)
                ConfigurationBuilder.update_vehicle_planning_problem_from_cart(vehicle, trajectory)
                vehicle.set_clcs_at_initial_step(t_start, vehicle.config.planning.CLCS)

                # For dealing with routing bug in route-planner
                ConfigurationBuilder.update_vehicle_reference_path(vehicle, target_state.convert_to_cr_initial_state())


        # update vehicle reach interface
        for vehicle in self.list_vehicles:
            vehicle.reach_interface.add_previous_multi_reachable_sets(vehicle.reach_interface.reachable_set)










    def _derive_target_state(self, t_start: int, t_end: int, vehicle_id: int, vehicle_under_test: AgentVehicle) -> np.array:
        """
        Cases:
        > none intersection
        > one intersection
        > multiple intersections
        """

        # find other vehicle
        other_vehicle = self.list_vehicles[vehicle_id]

        list_overlapping_regions_of_vehicle_id_at_t_end: List[OverlappingRegion] = OverlappingRegion.find_overlapping_regions(
            ego_vehicle=vehicle_under_test,
            other_vehicle=other_vehicle,
            step=t_end
        )

        match len(list_overlapping_regions_of_vehicle_id_at_t_end):
            case 0:
                # Follow reference path with const/max velocity, if no intersection available
                # TODO: decide velocity strategy
                print(f'could not find overlapping region for vehicle {vehicle_id}')
                target_state = TargetState(overlapping_region=None, vehicle=other_vehicle, time_step=t_end)
                target_state.heuristic_target_state_generation(t_start, t_end)
                return target_state

            case 1:
                print(f'found one overlapping region for vehicle {vehicle_id}')
                # Only one overlapping area used
                overlapping_region = list_overlapping_regions_of_vehicle_id_at_t_end[0]
                target_state = TargetState(overlapping_region, other_vehicle, time_step=t_end)
                target_state.derive_target_velocity_cvl()
                return target_state

            case _:
                print(f'found {len(list_overlapping_regions_of_vehicle_id_at_t_end)} overlapping regions for vehicle {vehicle_id}')
                # take centroid with biggest area
                overlapping_region: OverlappingRegion = sorted(list_overlapping_regions_of_vehicle_id_at_t_end,
                                                               key=lambda x: x.area, reverse=True)[0]
                target_state = TargetState(overlapping_region, other_vehicle, time_step=t_end)
                target_state.derive_target_velocity_cvl()
                return target_state


    def get_input_from_vut_vehicle(self, vut_vehicle: AgentVehicle, t_start: int, t_end: int, vut_planner="QP"):
        """
        The target vehicles reacts to its environment, either via planner or human driver.
        Choose dummy vut planner between A-Star and QP.
        """

        # DUMMY CODE FOR QP


        init_position_cvl = vut_vehicle.get_initial_position_cvl()
        init_vel_cvl = vut_vehicle.get_initial_velocity_cvl()
        initial_state = GeneralState(vut_vehicle, t_start)
        initial_state.init_state_from_curvilinear(p_lon=init_position_cvl[0], p_lat=init_position_cvl[1],
                                                  v_lon=init_vel_cvl[0], v_lat=init_vel_cvl[1])

        # get target state
        target_state = TargetState(overlapping_region=None, vehicle=vut_vehicle, time_step=t_end)
        target_state.vut_state_generation(t_start, t_end)

        # save target state in vehicle
        vut_vehicle.set_initial_state_for_step(initial_state, t_start)
        vut_vehicle.set_target_state_for_step(target_state, t_end)


        if(vut_planner == "QP"):
            planner = DummyQP(vut_vehicle, initial_state, target_state)
        else:
            raise NotImplementedError(f'vut_planner {vut_planner} not an option')

        planner.generate_trajectory()


        # Final config steps, necessary if you want to use your own planner
        vut_vehicle.set_trajectory_for_start_step(planner, t_start)
        ConfigurationBuilder.update_vehicle_planning_problem_from_cart(vut_vehicle, planner)
        vut_vehicle.reach_interface.add_previous_multi_reachable_sets(vut_vehicle.reach_interface.reachable_set)




    def check_collision_of_vut(self, t_start: int, t_end: int) -> Tuple[bool, List]:
        """
        Checks if a collision of the VUT with the attackers happened.
        Returns True if so and dict{attacker_vehicle: collision_polygon} as shapely polygon
        """
        # TODO: Better algorithm

        # Return values
        all_collisions_flag: bool = False
        list_all_collision: list = list()

        # VUT
        vut_vehicle: AgentVehicle = AgentVehicle.find_vehicle(is_vut=True)
        vut_trajectory: TargetTrajectory = vut_vehicle.get_trajectory(t_start)

        # For each attacker vehicle at each time step, check if there is an intersection
        for vehicle in self.list_vehicles:
            if(vehicle is vut_vehicle):
                # check for attackers
                continue

            attacker_trajectory: TargetTrajectory = vehicle.get_trajectory(t_start)
            collision_flag, list_collisions = CollisionFactory.check_collision(vut_trajectory, attacker_trajectory, t_start, t_end)
            if(collision_flag):
                all_collisions_flag = True
                list_all_collision.extend(list_collisions)
                warnings.warn(f'Found {len(list_collisions)} collision(s) between VUT vehicle and vehicle: {vehicle.id}')

        # save them in scenario manager
        for collision in list_all_collision:
            self.dict_step_to_collisions[collision.t_collision].append(collision)

        return all_collisions_flag, list_all_collision

