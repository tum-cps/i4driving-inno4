import os
import copy

from commonroad_reach.data_structure.configuration_builder import ConfigurationBuilder as CRReachConfigBuilder
from utils.inter_agent_traffic_rules.virtual_planning_problem import VirtualPlanningProblemFactory
from utils.scenario_modification.state_generation import AgentDynamicObstacleFactory, AgentInitialStateFactory, AgentStateFactory
from data_structures.configuration.scenario_configuration import ScenarioConfiguration


# Typing
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from data_structures.target_trajectory import TargetTrajectory
    from data_structures.agent_vehicle import AgentVehicle
    from data_structures.i4d_states import TargetState, GeneralState
    from typing import Dict, List
    from commonroad.scenario.state import CustomState as CR_CustomState
    from commonroad.scenario.state import InitialState as CR_InitialState



class ConfigurationBuilder:
    """
    Class to build individual configurations for each planning problem.
    """

    @classmethod
    def build_configurations(cls, name_scenario: str, path_root: str, consider_inter_agent_traffic_rules: bool = False) -> list["ScenarioConfiguration"]:
        """
        Summary
        -------
        Generates the config files for each seperate planning problem (i.e. each cooperative vehicle) from one
        initial config



        Args
        -----
        > name_scenario (str): _description_
        > path_root (str): path to root folder, that contains both /scenarios and /configurations folder.

        Returns
        -------
            list(ScenarioConfiguration): list of configs for each cooperative vehicel
        """

        # Integrity Check
        if (not os.path.exists(path_root) or 'configurations' not in next(os.walk(path_root))[1]):
            raise FileNotFoundError(f'path_root {path_root} must point to the root, i.e. to commonroad-reach-negotiation')

        config_base = CRReachConfigBuilder(path_root=path_root).build_configuration(name_scenario)

        config_coop = ScenarioConfiguration(config_base.config_omega)

        config_coop.update()
        config_coop.print_configuration_summary()

        # check timing
        if(config_coop.scenario.dt != config_coop.planning.dt):
            raise ValueError(f'Currently, scenario.dt from .xml file and planning.dt from config must match but'
                             f'scenario.dt={config_coop.scenario.dt} and planning.dt={config_coop.planning.dt}')

        # generate one specific config for each agent
        list_configs = config_coop.split_to_planning_problems()

        # consider agent-to-non-agent traffic rules
        list_configs = cls._assign_traffic_rules_to_vehicle_after_config_splitting(list_configs)

        # consider agent-to-agent-traffic rules
        if (consider_inter_agent_traffic_rules):
            list_configs = cls._add_virtual_dynamic_obstacles_to_configs_after_config_splitting(list_configs)

        return list_configs

    @classmethod
    def _assign_traffic_rules_to_vehicle_after_config_splitting(cls, list_configs: list[ScenarioConfiguration]) \
            -> list[ScenarioConfiguration]:
        """
        After splitting the config to individual planning problems, assign each of them traffic rules
        """
        # Call after config splitting and update() to assign traffic rules per vehicle

        list_configs_with_rules = []
        for config in list_configs:
            vehicle_id = "vehicle_" + str(config.planning_problem.planning_problem_id)
            traffic_rules = set()

            # Add common traffic rules for all vehicles
            traffic_rules.update(config.traffic_rule.rules_per_vehicle['vehicle_all']['activated_rules'])

            # Add special traffic rules for this vehicle
            if (vehicle_id in config.traffic_rule.rules_per_vehicle.keys()):
                traffic_rules.update(config.traffic_rule.rules_per_vehicle[vehicle_id]['activated_rules'])

            config.traffic_rule.activated_rules = list(traffic_rules)
            list_configs_with_rules.append(config)

        return list_configs_with_rules

    @classmethod
    def _add_virtual_dynamic_obstacles_to_configs_after_config_splitting(cls, list_configs: list[ScenarioConfiguration]) \
            -> list[ScenarioConfiguration]:
        # call after splitting to add virtual dynamic obstacles for other vehicles

        # 1. get all individual scenarios from config.scenario
        # 2. get all reference paths from config.planning.reference_path[:, 0] & config.planning.reference_path[:,1]
        # 3. for each vehicle, assign all other vehicles as dynamic obstacles following their reference path
        #   3.1. velocity const for now

        # for each agent, add all other agents as dynamic obstacles
        # Uses work-around for property decorator in commonroad

        # necessary to get rid of circle-reference of same object before modifying individual scenario
        for config in list_configs:
            config.scenario = copy.copy(config.scenario)

        for idx_ego, config_ego in enumerate(list_configs):
            dict_ego_obstacles = copy.copy(config_ego.scenario._dynamic_obstacles)
            obstacle_counter = len(dict_ego_obstacles.keys())
            for idx_all, config in enumerate(list_configs):
                if (idx_all != idx_ego):
                    virtual_obstacle = VirtualPlanningProblemFactory.create_virtual_dynamic_obstacle(config)
                    obstacle_counter += 1
                    dict_ego_obstacles.update({obstacle_counter: virtual_obstacle})

            config_ego.scenario._dynamic_obstacles = dict_ego_obstacles

        return list_configs

    @classmethod
    def add_attacking_vehicle_as_config_object(cls, vut_vehicle: "AgentVehicle", attacker_vehicle: "AgentVehicle",
                                               attacker_trajectory: "TargetTrajectory"):
        """
        Takes a trajectory object and adds a dynamic obstacle to the vut vehicle scenario
        """

        # config
        vut_config: ScenarioConfiguration = vut_vehicle.config
        attacker_config: ScenarioConfiguration = attacker_vehicle.config
        attacker_id: int = attacker_config.planning_problem.planning_problem_id

        # add id to config
        vut_config.list_attacking_vehicle_object_id.append(attacker_id)

        # trajectory
        trajectory_dict: Dict = attacker_trajectory.get_trajectory_dict()

        list_trajectory_states: List["CR_CustomState"] = list()
        for time_step, trajectory_state in trajectory_dict.items():
            if(time_step == attacker_trajectory.t_start):
                # create initial state object
                initial_state: "GeneralState" = trajectory_dict[attacker_trajectory.t_start]
                initial_state_obj: "CR_InitialState" = AgentInitialStateFactory.create_initial_state_from_cartesian(
                        position=initial_state.centroid_cart,
                        v_x=initial_state.v_x, v_y=initial_state.v_y, a_x=initial_state.a_x, a_y=initial_state.a_y,
                        time_step=time_step, vehicle=attacker_vehicle)

            # create custom state object for all states
            elif(time_step == attacker_trajectory.t_end):
                target_state: "TargetState" = trajectory_dict[attacker_trajectory.t_end]
                # target state has no acceleration
                target_state_obj: "CR_CustomState" = AgentStateFactory.create_state_from_cartesian(
                    position=target_state.centroid_cart,
                    v_x=target_state.v_x, v_y=target_state.v_y, a_x=0, a_y=0, time_step=time_step, vehicle=attacker_vehicle)

                list_trajectory_states.append(target_state_obj)

            else:
                state: "GeneralState" = trajectory_dict[time_step]
                state_obj: "CR_CustomState" = AgentStateFactory.create_state_from_cartesian(
                    position=state.centroid_cart,
                    v_x=state.v_x, v_y=state.v_y, a_x=state.a_x, a_y=state.a_y, time_step=time_step, vehicle=attacker_vehicle)
                list_trajectory_states.append(state_obj)

        # sort ascending by time step
        list_trajectory_states = sorted(list_trajectory_states, key=lambda x: x.time_step)



        # create dymaic obstacle
        dynamic_obstacle = AgentDynamicObstacleFactory.create_dynamic_obstacle(vut_config, initial_state_obj,
                                                                               list_trajectory_states, attacker_id)


        # update scenario
        obstacle_counter = len(vut_config.scenario.dynamic_obstacles) + 1
        vut_config.scenario._dynamic_obstacles.update({obstacle_counter: dynamic_obstacle})

    @classmethod
    def update_vehicle_planning_problem_from_cart(cls, attacker_vehicle: "AgentVehicle",
                                        attacker_trajectory):
        """
        Updates vehicle config with new planning_problem from cartesian trajectory
        """

        # config
        attacker_config: ScenarioConfiguration = attacker_vehicle.config
        # trajectory
        trajectory_dict: Dict = attacker_trajectory.get_trajectory_dict()

        # Target state from trajectory is new initial state
        new_initial_state: "GeneralState" = trajectory_dict[attacker_trajectory.t_end]
        new_initial_state_obj: "CR_InitialState" = AgentInitialStateFactory.create_initial_state_from_cartesian(
            position=new_initial_state.centroid_cart,
            v_x=new_initial_state.v_x, v_y=new_initial_state.v_y, a_x=0, a_y=0, time_step=new_initial_state.time_step,
            vehicle=attacker_vehicle)

        # manage cr
        setattr(new_initial_state_obj, "yaw_rate", 0)
        setattr(new_initial_state_obj, "slip_angle", 0)

        # modify planning problem so that the planning loop works
        attacker_config.planning_problem.initial_state = new_initial_state_obj
        attacker_config.planning_problem.initial_state.time_step = new_initial_state.time_step
        attacker_config.planning.p_lon_initial = new_initial_state.centroid_cvl[0]
        attacker_config.planning.p_lat_initial = new_initial_state.centroid_cvl[1]
        attacker_config.planning.v_lon_initial = new_initial_state.v_lon
        attacker_config.planning.v_lat_initial = new_initial_state.v_lat




    @classmethod
    def update_vehicle_reference_path(cls, vehicle: "AgentVehicle", initial_state_cartesian: "CR_InitialState") -> None:
        """
        Update vehicle reference path and CLCS in cartesian
        """

        scenario = vehicle.config.scenario
        goal_region = vehicle.config.planning.goal_region

        # Deals with try-except config update bug by trying it on a deepcopy of the config first
        try:
            copied_config = copy.deepcopy(vehicle.config)
            copied_config.update(scenario=scenario, state_initial=initial_state_cartesian, goal_region=goal_region)
        except:
            return

        vehicle.config.update(scenario=scenario, state_initial=initial_state_cartesian, goal_region=goal_region)



























