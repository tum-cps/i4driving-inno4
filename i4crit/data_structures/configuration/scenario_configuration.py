import logging
from typing import Union

from collections import defaultdict

from omegaconf import ListConfig, DictConfig
from commonroad_reach.data_structure.configuration import ConfigurationBase
from commonroad_reach_semantic.data_structure.config.semantic_configuration import SemanticConfiguration

logger = logging.getLogger(__name__)


class ScenarioConfiguration(SemanticConfiguration):
    """
    Configuration class for the entire scenario. Is later split into indiviudal planning problems by the configuration
    builder.
    """
    def __init__(self, config_omega: Union[ListConfig, DictConfig]):
        super().__init__(config_omega)

        self.config_omega = config_omega
        self.traffic_rule: MultiAgentTrafficRuleConfiguration = MultiAgentTrafficRuleConfiguration(config_omega)

        # Due to the cr, this has to be modified without attrs.
        self.list_attacking_vehicle_object_id = list()



class MultiAgentTrafficRuleConfiguration(ConfigurationBase):
    """
    A class that adds specific CR-Semantics configuration
    """
    def __init__(self, config: Union[ListConfig, DictConfig]):
        config_relevant = config.traffic_rules

        self.rules_per_vehicle = defaultdict(list)
        for k, v in config_relevant.items():
            if ("vehicle_" in k):
                self.rules_per_vehicle[k] = v

        asdf = config_relevant.keys()

        self.distance_braking = config_relevant.distance_braking
        self.acceleration_braking_hard = config_relevant.acceleration_braking_hard
        self.backward_driving_v_err = config_relevant.backward_driving_v_err
        self.activated_rules = None  # config_relevant.activated_rules
        self.mode_spot = config_relevant.mode_spot
        self.mode_automata = config_relevant.mode_automata