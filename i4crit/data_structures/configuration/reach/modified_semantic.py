import numpy as np


# third party
from omegaconf import ListConfig, DictConfig

# commonroad
from commonroad.scenario.scenario import Scenario
from commonroad.scenario.trajectory import State
from commonroad.planning.goal import GoalRegion
from commonroad.planning.planning_problem import PlanningProblem, PlanningProblemSet
from commonroad_dc.pycrccosy import CurvilinearCoordinateSystem
from commonroad_dc.geometry.util import resample_polyline
from commonroad_route_planner.route_planner import RoutePlanner
from commonroad_reach.utility import configuration as util_configuration
from commonroad_reach_semantic.data_structure.config.semantic_configuration import (SemanticConfiguration,
                                                                                    SemanticReachableSetConfiguration,
                                                                                    SONIAConfiguration,
                                                                                    SemanticModelConfiguration,
                                                                                    TrafficRuleConfiguration)

# own code


# Typing
from typing import Optional, Union, Tuple, List




class ModifiedSemanticConfiguration(SemanticConfiguration):

    def __init__(self, config_omega: Union[ListConfig, DictConfig]):
        super().__init__(config_omega)

    def update(self, scenario: Scenario = None, planning_problem_set: PlanningProblemSet = None,
               planning_problem: PlanningProblem = None, idx_planning_problem: int = 0,
               state_initial: State = None, goal_region: GoalRegion = None,
               CLCS: CurvilinearCoordinateSystem = None, list_ids_lanelets: List[int] = None):

        super().update(scenario, planning_problem_set, planning_problem, idx_planning_problem, state_initial,
                       goal_region, CLCS, list_ids_lanelets)


        # Modifications so that the edgy lane change problem is not occuring
        self._init_ref_path()
        self._smooth_reference_path()
        self._init_CLCS()


        super().update(scenario, planning_problem_set, planning_problem, idx_planning_problem, state_initial,
                       goal_region, CLCS, list_ids_lanelets)




    def _init_ref_path(self) -> None:
        """

        """
        # plans a route from the initial lanelet to the goal lanelet, set curvilinear coordinate system
        route_planner: RoutePlanner = RoutePlanner(lanelet_network=self.scenario.lanelet_network,
                                     planning_problem=self.planning_problem,
                                     state_initial=self.state_initial,
                                     goal_region=self.goal_region)
        candidate_holder = route_planner.plan_routes()
        route = candidate_holder.retrieve_first_route()

        if route:
            self.route = route
            self.reference_path: np.ndarray = resample_polyline(route.reference_path, 0.5)

        else:
            raise ValueError(f'Could not find route')




    def _smooth_reference_path(self):
        """
        Smooth reference path if it is too edgy using Chaikin's Corner Cutting Algorithm.
        """
        pass



    def _init_CLCS(self):
        """
        Generates reference path from CLCS
        """
        self.CLCS = util_configuration.create_curvilinear_coordinate_system(self.reference_path)
        self.reference_path = np.array(self.CLCS.reference_path())

        p_initial, v_initial = util_configuration.compute_initial_state_cvln(self, self.state_initial)

        self.p_lon_initial, self.p_lat_initial = p_initial
        self.v_lon_initial, self.v_lat_initial = v_initial

















