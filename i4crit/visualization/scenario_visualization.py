import copy
import logging
import math
import os
from pathlib import Path
from typing import Tuple, Union, List, Set

import imageio
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns


from commonroad.geometry.shape import Polygon, Rectangle, Circle
from commonroad.visualization.mp_renderer import MPRenderer
from commonroad.visualization.draw_params import MPDrawParams

import commonroad_reach.utility.logger as util_logger
from commonroad_reach.data_structure.reach.reach_interface import ReachableSetInterface
from commonroad_reach.utility import coordinate_system as util_coordinate_system


# Typing
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from data_structures.agent_vehicle import AgentVehicle
    from data_structures.target_trajectory import TargetTrajectory
    from data_structures.i4d_states import GeneralState, TargetState
    from commonroad.scenario.scenario import Scenario
    from data_structures.collision import Collision

from typing import List



logger = logging.getLogger(__name__)
logging.getLogger('PIL').setLevel(logging.WARNING)
logging.getLogger('matplotlib.font_manager').setLevel(logging.WARNING)


def convert_scenario_params(config, time_step):
    """
    Converts scenario params from config to commonroad 2023
    """
    draw_params = MPDrawParams()
    draw_params.dynamic_obstacle.draw_icon = config.debug.draw_icons
    draw_params.dynamic_obstacle.show_label = True
    draw_params.lanelet_network.lanelet.show_label = config.debug.draw_lanelet_labels
    draw_params.planning_problem.initial_state.state.draw_arrow = True
    draw_params.planning_problem.goal_region.draw_occupancies = True
    draw_params.time_begin = time_step
    return draw_params


def convert_planning_to_new_crio_param_version():
    """
    Converts planning params from config to commonroad 2023
    """
    draw_params = MPDrawParams()
    draw_params.planning_problem.initial_state.state.draw_arrow = True
    draw_params.planning_problem.initial_state.state.radius = 0.
    return draw_params


def convert_face_colors(edge_color, face_color):
    """
    Converts face color params from config to commonroad 2023
    """
    draw_params = MPDrawParams()
    draw_params.shape.facecolor = face_color
    draw_params.shape.edgecolor = edge_color
    return draw_params


def plot_scenario(list_vehicles: List["AgentVehicle"],
                                                 list_step_start: List[int], step_end: int,
                                                 dict_step_to_collisions: dict=None,
                                                 figsize: Tuple = None,
                                                 plot_limits: List = None, path_output: str = None,
                                                 save_gif: bool = True, duration: float = None, terminal_set=None):
    """
    Summary
    -------
    Plots scenario with computed reachable sets for cooperative vehicle.

    Args
    ----
    list_vehicles (list[AgentVehicle]): list of cooperative vehicles in the scenario, each containing all relevant information of their reachable sets
    step_start (int): start time step
    step_end (int): end time step
    steps (list[int]): list of steps in scenario
    """

    for vehicle in list_vehicles:
        if(vehicle.is_vut):
            vut_vehicle = vehicle
            break


    reach_interface = vut_vehicle.reach_interface
    config = reach_interface.config
    scenario = config.scenario
    list_reach_interfaces = [vehicle.reach_interface for vehicle in list_vehicles]

    # Create saving path
    path_output = path_output or config.general.path_output
    Path(path_output).mkdir(parents=True, exist_ok=True)

    # create figure
    figsize = figsize if figsize else (25, 15)
    plot_limits = plot_limits or compute_plot_limits_from_road_network(scenario)
    print(plot_limits)
    palette = sns.color_palette("viridis", len(list_vehicles))
    vut_palette = sns.color_palette("hls", 8)

    # Start and stop step
    step_start = list_step_start[0]
    multi_start_step: int = list_step_start[0]
    list_step_start.pop(-1)
    step_end = step_end

    steps = range(step_start, step_end + 1)

    # Time increment ?
    duration = duration if duration else config.planning.dt

    util_logger.print_and_log_info(logger, "* Plotting reachable sets...")
    renderer = MPRenderer(plot_limits=plot_limits, figsize=figsize) if config.debug.save_plots else None

    # For each step in the planning problem
    for step in steps:

        # set multistart step for plotting the different planning problem trajectories
        if (step in list_step_start and multi_start_step != step):
            multi_start_step = step

        time_step = step * round(config.planning.dt / config.scenario.dt)
        if config.debug.save_plots:
            # clear previous plot
            plt.cla()
        else:
            # create new figure
            plt.figure(figsize=figsize)
            renderer = MPRenderer(plot_limits=plot_limits)

        draw_params = convert_scenario_params(config, time_step)

        # plot scenario and planning problem
        scenario.draw(renderer, draw_params=draw_params)

        # set current step start
        if(step in list_step_start):
            multi_start_step: int = step


        # For each cooperative vehicle
        for idx, vehicle in enumerate(list_vehicles):

            # Get config for each vehicle (including curvilinear system)
            config = vehicle.reach_interface.config
            planning_problem = config.planning_problem
            ref_path = config.planning.reference_path  # reference path

            # attacker vehicles change clcs, visualization need that here
            if(vehicle.is_vut):
                current_CLCS = vehicle.config.planning.CLCS
            else:
                current_CLCS = vehicle.get_clcs_at_step(step)


            if config.debug.draw_planning_problem:
                draw_params = convert_planning_to_new_crio_param_version()
                planning_problem.draw(renderer, draw_params=draw_params)

            if(vehicle.is_vut):
                face_color = (vut_palette[0][0] * 0.75, vut_palette[0][1] * 0.75, vut_palette[0][2] * 0.75)
                edge_color = (vut_palette[0][0] * 0.75, vut_palette[0][1] * 0.75, vut_palette[0][2] * 0.75)
            else:
                face_color = (palette[idx][0] * 0.75, palette[idx][1] * 0.75, palette[idx][2] * 0.75)
                edge_color = (palette[idx][0] * 0.75, palette[idx][1] * 0.75, palette[idx][2] * 0.75)

            draw_params = convert_face_colors(edge_color, face_color)
            # Get reachable set
            list_nodes = vehicle.reach_interface.multi_iteration_reachable_set_at_step(step)
            draw_reachable_sets(list_nodes, config, current_CLCS, renderer, draw_params)

            # Draw overlapping area
            list_overlapping_regions = vehicle.dict_step_to_overlap[step]
            draw_overlapping_regions(list_overlapping_regions, renderer, draw_params)

            # draw target_states
            target_state = vehicle.get_target_state_for_end_step(step)
            if(target_state is not None):
                draw_target_states(target_state, renderer, draw_params)

            # draw trajectory
            trajectory: TargetTrajectory = vehicle.get_trajectory(multi_start_step)
            if(trajectory is not None):
                trajectory_dict = trajectory.get_trajectory_dict()
                if(step in trajectory_dict.keys()):
                    trajectory_point = trajectory_dict[step]
                    draw_trajectory(trajectory_point, renderer, draw_params, is_vut=vehicle.is_vut)

            # draw collisions
            if(dict_step_to_collisions is not None and not vehicle.is_vut):
                if(step in dict_step_to_collisions.keys()):
                    print(f'Drawing collision')
                    collisions_at_step: List = dict_step_to_collisions[step]
                    draw_collision(collisions_at_step, renderer, draw_params)


            # plot reference path
            if config.debug.draw_ref_path and ref_path is not None:
                 renderer.ax.plot(ref_path[:, 0], ref_path[:, 1],
                                 color='g', marker='.', markersize=1, zorder=19, linewidth=2.0)

        # plot terminal set
        if terminal_set:
            terminal_set.draw(renderer,
                              draw_params={"shapel_polygon": {
                                  "opacity": 1.0,
                                  "linewidth": 0.5,
                                  "facecolor": "#f1b514",
                                  "edgecolor": "#302404",
                                  "zorder": 15}})

        # settings and adjustments
        plt.rc("axes", axisbelow=True)
        ax = plt.gca()
        ax.set_aspect("equal")
        ax.set_title(f"$t = {time_step / 10.0:.1f}$ [s]", fontsize=28)
        ax.set_xlabel(f"$s$ [m]", fontsize=28)
        ax.set_ylabel("$d$ [m]", fontsize=28)
        plt.margins(0, 0)
        renderer.render()

        if config.debug.save_plots:
            save_fig(save_gif, path_output, step)

        else:
            plt.show()

    if config.debug.save_plots and save_gif:
        make_gif(path_output, "png_reachset_", steps, str(scenario.scenario_id), duration)

    util_logger.print_and_log_info(logger, "\tReachable sets plotted.")

    if config.debug.save_config:
        config.save(path_output, str(scenario.scenario_id))
        util_logger.print_and_log_debug(logger, "\tConfiguration file saved.", verbose=False)



def draw_reachable_sets(list_nodes: list, config, current_CLCS, renderer, draw_params):
    """
    Draw reachable set as polygon
    """
    dp = copy.copy(draw_params)
    dp.shape.opacity = 0.3
    backend = "CPP" if config.reachable_set.mode_computation == 2 else "PYTHON"
    coordinate_system = config.planning.coordinate_system

    if coordinate_system == "CART":
        for node in list_nodes:
            vertices = node.position_rectangle.vertices if backend == "PYTHON" else node.position_rectangle().vertices()
            Polygon(vertices=np.array(vertices)).draw(renderer, draw_params=dp)

    elif coordinate_system == "CVLN":
        for node in list_nodes:
            position_rectangle = node.position_rectangle if backend == "PYTHON" else node.position_rectangle()
            list_polygons_cart = util_coordinate_system.convert_to_cartesian_polygons(position_rectangle,
                                                                                      current_CLCS, True)
            for polygon in list_polygons_cart:
                # TODO: bug with dict
                Polygon(vertices=np.array(polygon.vertices)).draw(renderer, draw_params=dp)


def draw_overlapping_regions(list_overlapping_regions, renderer, draw_params):
    """
    Draw overlapping region as light red, opace polygon
    """
    dp = copy.copy(draw_params)
    dp.shape.edgecolor = sns.color_palette("hls", 8)[0]
    dp.shape.facecolor = sns.color_palette("hls", 8)[0]
    dp.shape.opacity = 0.8
    for overlapping_region in list_overlapping_regions:
        Polygon(vertices=np.array(overlapping_region.vertices)).draw(renderer, draw_params=dp)


def draw_target_states(target_state, renderer, draw_params):
    """
    Draw target state as pink, opace polygon
    """
    dp = copy.copy(draw_params)
    dp.shape.edgecolor = sns.color_palette("hls", 8)[7]
    dp.shape.facecolor = sns.color_palette("hls", 8)[7]
    dp.shape.opacity = 0.8

    center = target_state.centroid_cart
    width = 2
    length = 2
    Rectangle(length, width, center).draw(renderer, draw_params=dp)



def draw_trajectory(trajectory_point: "GeneralState | TargetState", renderer, draw_params, is_vut=False):
    """
    Draw trajectory states as blue points
    """

    center = trajectory_point.centroid_cart

    orientation = math.atan2(trajectory_point.v_y, trajectory_point.v_x)



    if(not is_vut):
        width = 0.5
        length = 0.5
        draw_params.shape.facecolor = None  # default color#
    else:
        width = 2
        length = 4
        draw_params.shape.facecolor = '#ff0000'  # default color#

    Rectangle(length, width, center, orientation=orientation).draw(renderer, draw_params=draw_params)




def draw_collision(list_collisions_at_step: List["Collision"], renderer, draw_params) -> None:
    """
    Draw collisions as yellow circles
    """
    dp = copy.copy(draw_params)
    dp.shape.edgecolor = sns.color_palette("Paired", 11)[10]
    dp.shape.facecolor = sns.color_palette("Paired", 11)[10]
    dp.shape.opacity = 0.8


    radius: float = 2.0
    for collision in list_collisions_at_step:
        Circle(radius, collision.centroid_cart).draw(renderer, draw_params=draw_params)







def compute_plot_limits_from_road_network(scenario: "Scenario", margin: int = 20) -> list[float]:
    """
    Computes plot limits from scenario. Returns [xmin, xmax, ymin, ymax]
    """

    road_network_shapely = scenario.lanelet_network.lanelet_polygons

    # find max bounds of road network by iterating through its polygons.
    x_min = np.inf
    y_min = np.inf
    x_max = -np.inf
    y_max = -np.inf

    for polygon in road_network_shapely:
        pol_x_min = polygon.shapely_object.bounds[0]
        pol_y_min = polygon.shapely_object.bounds[1]
        pol_x_max = polygon.shapely_object.bounds[2]
        poly_y_max = polygon.shapely_object.bounds[3]

        x_min = min(x_min, pol_x_min)
        y_min = min(y_min, pol_y_min)
        x_max = max(x_max, pol_x_max)
        y_max = max(y_max, poly_y_max)


    boundaries: list = [x_min - margin, x_max + margin, y_min - margin, y_max + margin]

    return boundaries











def compute_plot_limits_from_reachable_sets(list_reach_interface: List[ReachableSetInterface], margin: int = 20):
    """
    Returns plot limits from the computed reachable sets.

    :param list_reach_interface: interface holding the computed reachable sets.
    :param margin: additional margin for the plot limits.
    :return:
    """
    x_min = y_min = np.infty
    x_max = y_max = -np.infty

    for reach_interface in list_reach_interface:
        config = reach_interface.config
        backend = "CPP" if config.reachable_set.mode_computation == 2 else "PYTHON"
        coordinate_system = config.planning.coordinate_system

        if coordinate_system == "CART":
            for step in range(reach_interface.step_start, reach_interface.step_end):
                for rectangle in reach_interface.drivable_area_at_step(step):
                    bounds = rectangle.bounds if backend == "PYTHON" else (rectangle.p_lon_min(), rectangle.p_lat_min(),
                                                                           rectangle.p_lon_max(), rectangle.p_lat_max())
                    x_min = min(x_min, bounds[0])
                    y_min = min(y_min, bounds[1])
                    x_max = max(x_max, bounds[2])
                    y_max = max(y_max, bounds[3])

        elif config.planning.coordinate_system == "CVLN":
            for step in range(reach_interface.step_start, reach_interface.step_end):
                for rectangle_cvln in reach_interface.drivable_area_at_step(step):
                    list_rectangles_cart = util_coordinate_system.convert_to_cartesian_polygons(rectangle_cvln,
                                                                                                config.planning.CLCS,
                                                                                                False)
                    for rectangle_cart in list_rectangles_cart:
                        bounds = rectangle_cart.bounds
                        x_min = min(x_min, bounds[0])
                        y_min = min(y_min, bounds[1])
                        x_max = max(x_max, bounds[2])
                        y_max = max(y_max, bounds[3])

    if np.inf in (x_min, y_min) or -np.inf in (x_max, y_max):
        return None

    else:
        return [x_min - margin, x_max + margin, y_min - margin, y_max + margin]


def make_gif(path: str, prefix: str, steps: Union[range, List[int]],
             file_save_name="animation", duration: float = 0.1):
    """
    Make gif from png.
    """
    images = []
    filenames = []

    util_logger.print_and_log_info(logger, "\tCreating GIF...")
    for step in steps:
        im_path = os.path.join(path, prefix + "{:05d}.png".format(step))
        filenames.append(im_path)

    for filename in filenames:
        images.append(imageio.imread(filename))

    imageio.mimsave(os.path.join(path, "../", file_save_name + ".gif"), images, duration=duration)


def save_fig(save_gif: bool, path_output: str, time_step: int):
    """
    Save figures. If save_fig: png for gifs, else format is svg.
    """
    if save_gif:
        # save as png
        print("\tSaving", os.path.join(path_output, f'{"png_reachset"}_{time_step:05d}.png'))
        plt.savefig(os.path.join(path_output, f'{"png_reachset"}_{time_step:05d}.png'), format="png",
                    bbox_inches="tight",
                    transparent=False)

    else:
        # save as svg
        print("\tSaving", os.path.join(path_output, f'{"svg_reachset"}_{time_step:05d}.svg'))
        plt.savefig(f'{path_output}{"svg_reachset"}_{time_step:05d}.svg', format="svg", bbox_inches="tight",
                    transparent=False)

