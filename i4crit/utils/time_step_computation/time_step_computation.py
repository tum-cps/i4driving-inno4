import math
from abc import ABC

import copy
import numpy as np

# third party
from shapely import LineString

# own libs
from data_structures.agent_vehicle import AgentVehicle
from data_structures.reach.fast_reach_polygon import FastReachPolygon

# typing
from typing import List, Tuple
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from data_structures.configuration.scenario_configuration import ScenarioConfiguration
    from data_structures.agent_vehicle import AgentVehicle


class TimeStepGenerator(ABC):
    """
    Class that generates the time steps, per default dynamically
    """

    @classmethod
    def _compute_start_and_end_step(cls, list_vehicles: List["AgentVehicle"],
                                    t_start:int=None, t_end:int=None) -> Tuple[int, int]:
        """
        First step is generated intersecting reference paths or smallest distance
        between points. Returns (time_start, time_end)
        """

        # find first start and last end
        time_start:int = np.inf
        time_end:int = -np.inf
        for vehicle in list_vehicles:
            copied_config: "ScenarioConfiguration" = copy.deepcopy(vehicle.config)

            if(copied_config.planning.step_start < time_start):
                time_start = copied_config.planning_problem.initial_state.time_step

            if(copied_config.planning.step_start + copied_config.planning.steps_computation > time_end):
                time_end = copied_config.planning.step_start + copied_config.planning.steps_computation


        if(t_end is not None):
            time_end: int = t_end

        if(t_start is not None):
            time_start:int = t_start

        return time_start, time_end




    @classmethod
    def generate_time_steps_from_user_input(cls, time_steps: list) -> list:
        """
        Generates the time steps specified by the user
        """
        time_steps = sorted(time_steps, reverse=False)
        return time_steps





    @classmethod
    def generate_equal_time_steps(cls, list_vehicles: List["AgentVehicle"], t_start:int=None,
                                  t_first: int=None, t_end:int=None, step_size:int=10) -> List[int]:
        """
        Generates equal time steps. Adds the modulo remainder to first step, if it exists.
        If t_end is not given, it uses the scenario time as end.

        If t_first is given, the rest of the steps are used with equal step size and might overshoot t_end
        """
        time_start, time_end = cls._compute_start_and_end_step(list_vehicles, t_start, t_end)

        if(t_start is not None):
            time_start = t_start
        if(t_end is not None):
            time_end = t_end



        # if t_first is not set use equal time steps without further intelligence
        if(t_first is None):
            remainder: int = (time_end - time_start) % step_size
            if(remainder != 0):
                # add remainder if division has modulo
                steps: list = list(range(time_start, time_end, step_size))
                el_zero = steps[0]
                steps = [step + remainder for step in steps if step != time_start]
                steps.insert(0, el_zero)
            else:
                steps: list = list(range(time_start, time_end + step_size, step_size))


        # if t_first is set, use step size from there -> t_end might be bigger as you want
        else:
            steps: list = [time_start, t_first]
            step = t_first + step_size

            while(step < t_end + step_size):
                steps.append(step)
                step += step_size

        return steps



    @classmethod
    def generate_time_steps_from_heuristic_doubled_first_step(cls, list_vehicles: List["AgentVehicle"], t_end:int=40, step_size=10) -> List[int]:
        """
        Generates time steps etc. from heuristic: first step is double the other steps
        """

        steps = cls.generate_equal_time_steps(list_vehicles, t_end=t_end, step_size=step_size)
        steps.pop(1)

        return steps




    @classmethod
    def generate_time_steps_from_heuristic_intersecting_ref_paths(cls, list_vehicles: List["AgentVehicle"],
                                                                  t_end:int=40,
                                                                  step_size:int=10,
                                                                  first_offset:int = 3) -> List[int]:
        """
        Generates a fast reach node approximation to approximate first intersecting time steps.
        Adds an offset to cope with the hardcore overapproximation
        """

        # Heuristic
        # ---------
        # 1. Check if an attacker has an intersecting, but not equal reference path
        # 2.A.1 If so, do a very simplified drivable area propagation to find the first step they intersect with the vut
        # 2.A.2 Add the first_offset to give more time for the actual reachable sets to find it
        # 2.B.1 If no intersecting reference paths --> NOT IMPLEMENTED YET


        # find vut_vehicle
        vut_vehicle = AgentVehicle.find_vehicle(is_vut=True)
        vut_ref_path: LineString = cls._convert_line_numpy_to_shapely(vut_vehicle.config.planning.reference_path)

        # check if there is an intersecting ref_path, i.e. ref_paths do not start at the same point but intersect
        found_intersect: bool = False
        intersecting_vehicle: AgentVehicle = None

        # other cases
        vut_point_cart = vut_vehicle.config.planning_problem.initial_state.position

        closest_on_non_intersecting_parallel_vehicle: AgentVehicle = None
        smallest_distance_of_parallel_vehicle: float = np.inf

        closest_same_refpath_vehicle: AgentVehicle = None
        smallest_distance_same_refpath_vehicle: float = np.inf



        for vehicle in list_vehicles:
            if(vehicle.is_vut):
                continue

            # FIXME: the case is missing, where one ref-path is a true Teilmenge of the other

            # check for non-equal ref paths
            if(not np.array_equal(vehicle.config.planning.reference_path[0:], vut_vehicle.config.planning.reference_path[0:])):
                attacker_ref_path: LineString = cls._convert_line_numpy_to_shapely(vehicle.config.planning.reference_path)

                # check for  intersecting non-equal reference paths
                if(vut_ref_path.intersects(attacker_ref_path)):
                    found_intersect = True
                    intersecting_vehicle = vehicle
                    break


                # check for closest non-intersecting on parallel path
                else:
                    vehicle_position_cart: np.ndarray = vut_vehicle.config.planning_problem.initial_state.position
                    distance: float = math.sqrt((vut_point_cart[0]-vehicle_position_cart[0])**2 +
                                         (vut_point_cart[0]-vut_point_cart[1])**2)

                    if(distance < smallest_distance_of_parallel_vehicle):
                        smallest_distance_of_parallel_vehicle = distance
                        closest_on_non_intersecting_parallel_vehicle = vehicle


            # check closest on equal ref path
            else:
                vehicle_position_cart: np.ndarray = vut_vehicle.config.planning_problem.initial_state.position
                distance: float = math.sqrt((vut_point_cart[0] - vehicle_position_cart[0]) ** 2 +
                                            (vut_point_cart[0] - vut_point_cart[1]) ** 2)

                if (distance < smallest_distance_same_refpath_vehicle):
                    smallest_distance_same_refpath_vehicle = distance
                    closest_same_refpath_vehicle = vehicle


        # if an intersecting reference path is found
        if(found_intersect):
            t_first: int = cls._fast_reach_intersection_time(vut_vehicle, intersecting_vehicle, t_end)
        else:
            # if no intersecting ref path is found
            # PRIORITY: first parallel then same ref path

            # Found vehicle on parallel ref-path
            if(closest_on_non_intersecting_parallel_vehicle is not None):
                t_first: int = cls._fast_reach_intersection_time(vut_vehicle,
                                                                 closest_on_non_intersecting_parallel_vehicle, t_end)

            # found vehicle on same ref path
            elif(closest_same_refpath_vehicle is not None):
                t_first: int = cls._fast_reach_intersection_time(vut_vehicle, closest_same_refpath_vehicle, t_end)

            else:
                raise NotImplementedError(f'This error might be the special case that the ref path of an attacker is a'
                                          f'strict subset (dt. echte Teilmenge) of the ref path of the VUT.')



        # integrate offset
        t_first += first_offset
        print(f'Found first intersection at {t_first}')

        # generate equal time steps but use first
        steps = cls.generate_equal_time_steps(list_vehicles, t_first=t_first, t_end=t_end, step_size=step_size)

        steps = sorted(steps, reverse=False)


        print(f'[Time Steps] Found time steps {steps}')

        return steps


    @classmethod
    def _fast_reach_intersection_time(cls, vut_vehicle: "AgentVehicle", attacker_vehicle: "AgentVehicle", t_end: int) -> int:
        """
        Make a fast overappoximation of the drivable area to find the first intersection time
        """

        t_first: int = None

        # Create faster reach_polygon class and initialize zero-state polygon
        vut_polygon: FastReachPolygon = FastReachPolygon(vut_vehicle)
        attacker_polygon: FastReachPolygon = FastReachPolygon(attacker_vehicle)
        vut_polygon.init_zero_state_polygon()
        attacker_polygon.init_zero_state_polygon()

        for step in range(t_end + 1):
            vut_polygon.propagate_polygon_one_step()
            attacker_polygon.propagate_polygon_one_step()

            # check if the overapproximated drivable areas of the vut and the attacker intersect at this time step,
            # if so, use that time step as first step

            """
            FastReachPolygon.debug_plot_polygon(
                                                vut_vehicle.config.scenario,
                                                vut_polygon.get_current_shapely_drivable_area_cart(),
                                                attacker_polygon.get_current_shapely_drivable_area_cart())
            """

            if (vut_polygon.get_current_shapely_drivable_area_cart().intersects(
                    attacker_polygon.get_current_shapely_drivable_area_cart())):
                t_first = step
                break


        if(t_first is None):
            raise ValueError(f'Could not find time step via fast reach_set propagation')

        return t_first











    @classmethod
    def _convert_line_numpy_to_shapely(cls, np_line: np.array) -> LineString:
        """
        Converts line from np.array to shapely LineString
        """
        coords = list()

        dim = np.shape(np_line)[0]
        for i in range(dim):
            point: Tuple = (np_line[i,0], np_line[i,1])
            coords.append(point)

        line = LineString(coords)
        return line












