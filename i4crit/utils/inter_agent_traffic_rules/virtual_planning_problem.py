import copy
from collections import defaultdict

import numpy as np
from commonroad.scenario.trajectory import Trajectory
from commonroad.scenario.obstacle import ObstacleType, DynamicObstacle
from commonroad.prediction.prediction import TrajectoryPrediction
from commonroad.geometry.shape import Rectangle
from data_structures.configuration.scenario_configuration import ScenarioConfiguration

from commonroad.scenario.state import CustomState, InitialState
from commonroad.planning.goal import GoalRegion

from scipy.spatial import KDTree
from math import atan2


# TODO: make this an actual factory, currently only concept


class VirtualStateFactory:
    """
    Factory for State objects for virtual dynamic obstacles of agents.
    """

    """
    Exemplary state definition

    <state>
        <position>
          <point>
            <x>3.1999</x>
            <y>3.5</y>
          </point>
        </position>
        <orientation>
          <exact>3.14</exact>
        </orientation>
        <time>
          <exact>48</exact>
        </time>
        <velocity>
          <exact>16.0</exact>
        </velocity>
     </state>
    """

    @classmethod
    def create_virtual_state(cls, position: (float, float), orientation_rad: float, time_step: int,
                             velocity: float, acceleration: float):
        state_dict = {
            "position": position,
            "orientation": orientation_rad,
            "time_step": time_step,
            "velocity": velocity,
            "acceleration": acceleration
        }

        return CustomState(**state_dict)


class VirtualPlanningProblemFactory:
    """
    Factory for modified Dynamic Obstacle instances for agent vehicles.
    """
    virtual_id_cnt: int = 20000
    width_rectangle: float = 0.1
    length_rectangle: float = 0.1
    velocity_factor: float = 1.2

    @classmethod

    def create_virtual_dynamic_obstacle(cls, config: ScenarioConfiguration):
        """
        Creates a virtual dynamic obstacle for all other agents then the current planning problem.
        The dynamic obstacles are used for inter-agent traffic rules.
        """
        id = cls.virtual_id_cnt
        cls.virtual_id_cnt += 1

        # Some useful stuff from the commonroad hell-hole
        planning_problem = config.planning_problem

        dt: float = config.planning.dt
        initial_step: int = config.planning.step_start
        final_step: int = config.planning.steps_computation

        initial_state: InitialState = planning_problem.initial_state
        initial_velocity: float = planning_problem.initial_state.velocity
        goal_state: GoalRegion = planning_problem.goal
        orientation: float = planning_problem.initial_state.orientation
        center = planning_problem.initial_state.position
        reference_path = config.planning.reference_path

        t_start = config.planning.step_start
        t_end = config.planning.steps_computation

        # Create shape class for vehicle
        obst_rect = Rectangle(cls.length_rectangle, cls.width_rectangle, center, orientation)

        # create fake trajectory with constant velocity
        dict_step_to_trajectory_point, dict_step_to_orientation = \
            cls._compute_trajectory_from_reference_path(reference_path, initial_state, goal_state, dt, initial_step,
                                                        final_step)

        # create prediction, whatever that is....?
        prediction = cls._create_trajectory_prediction(initial_state, goal_state, t_start, t_end, obst_rect,
                                                       dict_step_to_trajectory_point, dict_step_to_orientation)

        # create obstacle class for vehicle
        obstacle = DynamicObstacle(obstacle_id=id,
                                   obstacle_type=ObstacleType("car"),
                                   obstacle_shape=obst_rect,
                                   initial_state=initial_state,
                                   prediction=prediction)

        # add virtual agent as attribute, so the collision checker can find it later
        setattr(obstacle, 'is_virtual_agent', True)

        # set initial state of obstacle
        obstacle.initial_state = initial_state

        return obstacle

    @classmethod
    def _create_trajectory_prediction(cls, initial_state, goal_state,
                                      t_start, t_end, obst_rect,
                                      dict_step_to_trajectory_point: dict, dict_step_to_orientation: dict):
        # orientation = copy.copy(initial_state.orientation)
        velocity = initial_state.velocity
        acceleration = 0

        state_list = list()
        # Generate state list
        for t, trajectory_point in dict_step_to_trajectory_point.items():
            state_list.append(VirtualStateFactory.create_virtual_state(trajectory_point, dict_step_to_orientation[t], t,
                                                                       velocity, acceleration))
        # Generate Trajectory class
        trajectory = Trajectory(state_list[0].time_step, state_list)

        # Generate TrajectoryPrediction object. Whatever the difference is to a Trajectory object....
        rotated_shape = obst_rect.rotate_translate_local(-initial_state.position, -initial_state.orientation)
        prediction = TrajectoryPrediction(trajectory, rotated_shape)

        return prediction


    @classmethod
    def _compute_trajectory_from_reference_path(cls, reference_path: np.ndarray,
                                                initial_state: InitialState, goal_state: GoalRegion,
                                                dt: float, initial_step: int, final_step: int) -> dict:
        # creates a dict[time_step] with a trajectory position for each time step. Assumes constant velocity.

        ref_path: np.ndarray = copy.copy(reference_path)
        init_state: InitialState = copy.copy(initial_state)
        init_vel: float = init_state.velocity

        # TODO: Currently, this is not entirely physically correct
        # 1. find closest point on reference path for initial state
        # 2. assume constant velocity --> TODO: might be problematic if 0 at init
        # 3. for each subsequent point, find point with closest matching velocity via kdTree radius search

        # positions per step
        dict_step_to_trajectory_points = defaultdict()

        # orientation per step
        dict_step_to_orientation = defaultdict()

        # Find starting point of trajectory via KDTree search
        kd_tree: KDTree = KDTree(ref_path)
        query_point = np.expand_dims(init_state.position, 0)

        dict_step_to_trajectory_points[initial_step] = np.squeeze(query_point)

        distance, kd_tree_point_index = kd_tree.query(query_point)
        new_query_point = ref_path[kd_tree_point_index, :]
        new_ref_path = ref_path[kd_tree_point_index[0]:, :]

        dx: float = new_query_point[0][0] - query_point[0][0]
        dy: float = new_query_point[0][1] - query_point[0][1]
        new_orientation = atan2(dy, dx)
        dict_step_to_orientation[initial_step] = new_orientation

        # Create trajectory points (vel = const.) for entire horizon
        for t in range(initial_step + 1, final_step + 1):
            old_query_point = np.squeeze(copy.copy(new_query_point))

            # create circle radius and use it for kdtree search
            distance_circle: float = cls.velocity_factor * init_vel * dt
            new_kd_tree = KDTree(new_ref_path)
            distances, point_idxs = new_kd_tree.query(new_query_point, k=10, distance_upper_bound=distance_circle)

            # Find point that is furthest away from the current point.
            # Assumes init velocity to be an upper bound
            max_distance_index = np.argmax(distances[np.isfinite(distances)])

            # If no valid point is found for that velocity, use closest point
            if (np.array_equal(new_ref_path[max_distance_index, :], new_query_point)):
                distances, point_idxs = new_kd_tree.query(new_query_point, k=2)
                max_distance_index = np.argmax(distances[np.isfinite(distances)])

            # Create the query point and add it
            new_query_point = new_ref_path[max_distance_index, :]
            new_ref_path = new_ref_path[max_distance_index:, :]

            # orientation
            dx: float = new_query_point[0] - old_query_point[0]
            dy: float = new_query_point[1] - old_query_point[1]
            new_orientation = atan2(dy, dx)
            dict_step_to_orientation[t] = new_orientation

            dict_step_to_trajectory_points[t] = np.squeeze(new_query_point)

        return dict_step_to_trajectory_points, dict_step_to_orientation





