import numpy as np
import warnings


from commonroad_reach.data_structure.reach.reach_node import ReachNode
from shapely import Polygon, LineString, Point
from shapely import intersection as shapely_intersection
from shapely.ops import nearest_points
import utils.cr_change_management.curvilinear_cartesian as curv_cart


# typing
from typing import List, Tuple
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from data_structures.agent_vehicle import AgentVehicle


def find_reach_node_with_highest_velocity_range(position_cvl: np.array, list_reach_nodes: List[ReachNode]) -> ReachNode:
    """
    check a list of reach node, which reach_nodes contain the position in curvilinear cosys and return the reach node,
    with the highest velocity range
    """

    shapely_point = Point(position_cvl[0], position_cvl[1])
    list_reach_nodes_containing_position = list()

    for reach_node in list_reach_nodes:
        if(reach_node.position_rectangle.shapely_object.contains(shapely_point)):
            list_reach_nodes_containing_position.append(reach_node)

    # find node with biggest curvilinear velocity difference
    node_delta_v_max = sorted(list_reach_nodes_containing_position, key=lambda x: (x.v_lon_max - x.v_lon_min), reverse=True)[0]

    return node_delta_v_max



def find_reach_node_with_minimal_distance_to_point(position_cvl: np.array, list_reach_nodes: List[ReachNode]) -> ReachNode:
    """
    Finds the closest reach node for a given point in curvilinear frame using the outer ring of the reach polygon.
    """

    if(len(list_reach_nodes) == 0):
        raise ValueError(f'list reach nodes is empty')

    shapely_point = Point(position_cvl[0], position_cvl[1])

    smallest_distance: float = np.inf
    closest_reach_node: ReachNode = None

    for reach_node in list_reach_nodes:
        # compute distance of point to reach node
        distance = reach_node.position_rectangle.shapely_object.distance(shapely_point)

        if(distance is not np.nan):
            # save closest reach node
            if(distance < smallest_distance):
                smallest_distance = distance
                closest_reach_node = reach_node


    # sanity check
    if(closest_reach_node is None):
        raise ValueError(f'Could not find closest reach node for cvl-point {position_cvl} during heuristic computation of target state')


    return closest_reach_node



def interpolate_velocity_by_cvl_position_value(position_cvl: float, reach_node: ReachNode,
                                               dimension: str) -> float:
    """
    Interpolate velocity in curvilinear domain for a given position.
    A 2D-shapely_polygon has the dimensions (position, velocity)
    """

    if(dimension not in ['lon', 'lat']):
        raise ValueError(f'dimension {dimension}, but must be in either lon or lat')


    # get shapely shapely_polygon
    if(dimension == 'lon'):
        shapely_polygon: Polygon = reach_node.polygon_lon.shapely_object
        v_max = reach_node.polygon_lon.v_max
        v_min = reach_node.polygon_lon.v_min
    else:
        shapely_polygon: Polygon = reach_node.polygon_lat.shapely_object
        v_max = reach_node.polygon_lat.v_max
        v_min = reach_node.polygon_lat.v_min


    #construct line
    line = LineString([[position_cvl, v_min - 1], [position_cvl, v_max + 1]])
    intersection: LineString = shapely_intersection(shapely_polygon, line)

    # get velocity intersections
    v_0 = intersection.xy[1][0]
    v_1 = intersection.xy[1][1]

    # interpolate
    v_inter = (v_0 + v_1)/2

    return v_inter


def interpolate_velocity_by_cvl_position_and_orientation(position_cvl: np.array,
                                                         reach_node: ReachNode,
                                                         v_lat_zero=False) -> Tuple[float,float]:
    """
    Interpolate orientation by curvilinear position so that the orientation_rad is achieved, if possible.
    If not possible, chose closest approximation.
    Currently assumes: goal orientation = 0
    """


    shapely_polygon_lon: Polygon = reach_node.polygon_lon.shapely_object
    v_lon_max = reach_node.polygon_lon.v_max
    v_lon_min = reach_node.polygon_lon.v_min

    shapely_polygon_lat: Polygon = reach_node.polygon_lat.shapely_object
    v_lat_max = reach_node.polygon_lat.v_max
    v_lat_min = reach_node.polygon_lat.v_min


    #construct line and cut polygons
    line_lon = LineString([[position_cvl[0], v_lon_min - 1], [position_cvl[0], v_lon_max + 1]])
    intersection_lon: LineString = shapely_intersection(shapely_polygon_lon, line_lon)

    line_lat = LineString([[position_cvl[1], v_lat_min - 1], [position_cvl[1], v_lat_max + 1]])
    intersection_lat: LineString = shapely_intersection(shapely_polygon_lat, line_lat)

    # get velocity intersections
    v_lon_0 = intersection_lon.xy[1][0]
    v_lon_1 = intersection_lon.xy[1][1]

    v_lat_0 = intersection_lat.xy[1][0]
    v_lat_1 = intersection_lat.xy[1][1]

    # interpolated v_lon
    v_lon_inter = (v_lon_0 + v_lon_1) / 2

    # interpolate v_lat
    if(v_lat_zero):
        # try to get v_lat close to zero, so that orientation is near reference pat
        if(v_lat_0 <= 0 and v_lat_1 >= 0):
            v_lat_inter:float = 0.0

        else:
            v_lat_inter = (v_lat_0 + v_lat_1) / 2



    else:
        # FIXME: variable orientation
        # find closest v_lat to 0
        v_lat_inter = (v_lat_0 + v_lat_1) / 2





    print(f'v_lat_0 {v_lat_0}  --  vlat_1 {v_lat_1}  --  v_lat_inter{v_lat_inter}')


    return v_lon_inter, v_lat_inter





def get_center_point_of_reach_node(reach_node: ReachNode, vehicle: "AgentVehicle") -> np.array:
    """
    Takes a reachnode and returns point as position array in curvilinear frame that is both inside the road
    and inside the reach node.
    """

    centroid_shapely_point: Point = reach_node.position_rectangle.shapely_object.centroid

    centroid_cvl: np.array = np.asarray([centroid_shapely_point.xy[0][0], centroid_shapely_point.xy[1][0]])


    return centroid_cvl



def get_point_in_reach_node(reach_node: ReachNode, p_lon, p_lat) -> np.array:
    """
    Takes a point, check if it is inside the reach node. If not, it returns the centroid instead
    """

    point_cvl = Point([p_lon, p_lat])


    if(reach_node.position_rectangle.shapely_object.contains(point_cvl)):
        return np.array([p_lon, p_lat])
    else:
        # Interpolate between closest point in polygon and middle
        polygon_center_cvl = get_center_point_of_reach_node(reach_node, None)

        closest_point_in_polygon, _ = nearest_points(reach_node.position_rectangle.shapely_object, point_cvl)

        p_lon_interpolate = (polygon_center_cvl[0] + closest_point_in_polygon.xy[0][0])/2
        p_lat_interpolate = (polygon_center_cvl[1] + closest_point_in_polygon.xy[1][0])/2

        return np.asarray([p_lon_interpolate, p_lat_interpolate])




        #return get_center_point_of_reach_node(reach_node, None)





def new_get_point_inside_reach_node_and_road_network(reach_node: ReachNode, vehicle: "AgentVehicle") -> np.array:
    """
    Takes a reachnode and returns point as position array in curvilinear frame that is both inside the road
    and inside the reach node
    """

    #centroid_shapely_point: Point = reach_node.position_rectangle.shapely_object.centroid

    intersected_polygon: Polygon = None

    for lane_polygon in vehicle.config.scenario.lanelet_network.lanelet_polygons:
        # convert lanelet to curvilinear frame
        lane_polygon_cart = curv_cart.convert_shapely_polygon_cartesian_to_curvilinear(vehicle, lane_polygon.shapely_object)

        # find first intersection
        if(lane_polygon_cart.intersects(reach_node.position_rectangle.shapely_object)):
            intersected_polygon = lane_polygon_cart.intersection(reach_node.position_rectangle.shapely_object)
            break

    if(intersected_polygon is None):
        raise ValueError(f'Intersected Polygon not found')

    centroid_cvl: np.array = np.asarray([intersected_polygon.centroid.xy[0][0], intersected_polygon.centroid.xy[1][0]])


    return centroid_cvl








