import copy
import os
import warnings
import math

import numpy as np

# commonroad
from commonroad.common.file_reader import CommonRoadFileReader
from commonroad.common.file_writer import CommonRoadFileWriter, FileFormat
from commonroad.common.writer.file_writer_interface import OverwriteExistingFile
from commonroad.common.util import Interval
from commonroad.geometry.shape import Rectangle
from commonroad.planning.goal import GoalRegion
from commonroad.planning.planning_problem import PlanningProblem, PlanningProblemSet
from commonroad.scenario.state import CustomState, InitialState


# Typing
from typing import List, Tuple, Dict
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from commonroad.scenario.scenario import Scenario
    from commonroad.planning.planning_problem import PlanningProblemSet
    from commonroad.scenario.obstacle import DynamicObstacle
    from commonroad.scenario.lanelet import Lanelet






class MultiAgentScenarioGenerator:
    """
    Generates a multi-agent scenario from a single agent scenario
    """


    def __init__(self, scenario_name: str,
                 input_dir:str = os.getcwd() + "/scenario_data_root/single_agent_scenarios",
                 output_dir:str = os.getcwd() + "/scenario_data_root/scenarios",
                 max_search_steps: int = 1000):


        # max search steps for breadth first
        self.max_search_steps = max_search_steps

        self.scenario_name: str = scenario_name


        # Paths and dirs
        if(not os.path.exists(input_dir)):
            raise FileNotFoundError(f'input directory not found: {input_dir}')
        if(not os.path.exists(output_dir)):
            raise FileNotFoundError(f'output directory not found: {output_dir}')

        self.input_dir: str = input_dir
        self.output_dir: str = output_dir
        self.file_path: str = os.path.join(input_dir,  scenario_name + ".xml")

        # Commonroad types
        self.scenario: "Scenario" = None
        self.planning_problem_set_original: "PlanningProblemSet" = None



    def _check_dyn_obstacle_init_near_planning_problem_init(self,
                                                            planning_problem: "PlanningProblem",
                                                            dyn_obstacle: "DynamicObstacle",
                                                            radius: float) -> bool:
        """
        Checks whether the initial state is inside the radius from the planning problem center
        """
        planning_problem_center: np.ndarray = planning_problem.initial_state.position
        dyn_obstacle_center: np.ndarray = dyn_obstacle.initial_state.position

        distance: float = math.sqrt((planning_problem_center[0] - dyn_obstacle_center[0])**2 +
                                    (planning_problem_center[1] - dyn_obstacle_center[1])**2)


        if(distance <= radius):
            return True
        else:
            return False









    def _check_planning_problem_dyn_obstacle_on_connected_road_parts(self, planning_problem: "PlanningProblem",
                                                                     dyn_obstacle: "DynamicObstacle") -> bool:
        """
        Checks if the lanelet of the dynamic obstacle and the planning problem are connected. Returns true if so.
        """
        planning_problem_start_position_cart: np.ndarray = planning_problem.initial_state.position
        dyn_obstacle_start_position_cart: np.ndarray = dyn_obstacle.initial_state.position
        lanelet_planning_problem_id: int = self.scenario.lanelet_network.find_lanelet_by_position([planning_problem_start_position_cart])[0][0]
        lanelet_dyn_obstacle_id: int = self. scenario.lanelet_network.find_lanelet_by_position([dyn_obstacle_start_position_cart])[0][0]
        lanelet_planning_problem: "Lanelet" = self.scenario.lanelet_network.find_lanelet_by_id(lanelet_planning_problem_id)
        lanelet_dyn_obstacle: "Lanelet" = self.scenario.lanelet_network.find_lanelet_by_id(lanelet_dyn_obstacle_id)

        return self._breadth_first_search_algorithm(lanelet_planning_problem, lanelet_dyn_obstacle)



    def _breadth_first_search_algorithm(self, lanelet_a: "Lanelet", lanelet_b: "Lanelet") -> bool:
        """
        Mock breadth-first search with visited queue.
        """
        visited_list: List = list()
        open_list_a: List["Lanelet"] = list()

        # initial state of search
        open_list_a, visited_list = self._append_adjecent_lanelets(open_list_a, lanelet_a, visited_list)
        if(lanelet_b in open_list_a):
            return True
        elif(len(open_list_a) == 0):
            return False

        # search until connection between start lanelet and goal lanelet are given
        cycle_count = 0
        for lanelet in open_list_a:
            open_list_a, visited_list = self._append_adjecent_lanelets(open_list_a, lanelet,
                                                                               visited_list)
            if (lanelet_b in open_list_a):
                return True
            elif (len(open_list_a) == 0):
                return False

            cycle_count += 1
            if(cycle_count > self.max_search_steps):
                raise RecursionError(f'Dynamic obstacle generation search did not terminate')




    def _append_adjecent_lanelets(self, list_lanelets: List["Lanelet"],
                                  lanelet: "Lanelet", exclude_list: List["Lanelet"]) -> Tuple[List["Lanelet"], List["Lanelet"]]:
        """
        Appends lanelets if they are not in exclude-list and deals with commonroad
        """

        # deal with commonroad
        if(len(lanelet.successor) > 0):
            for lanelet_id in lanelet.successor:
                successor: "Lanelet" = self.scenario.lanelet_network.find_lanelet_by_id(lanelet_id)
                if(successor not in exclude_list):
                    list_lanelets.append(successor)
        if(len(lanelet.predecessor) > 0):
            for lanelet_id in lanelet.predecessor:
                predecessor: "Lanelet" = self.scenario.lanelet_network.find_lanelet_by_id(lanelet_id)
                if (predecessor not in exclude_list):
                    list_lanelets.append(predecessor)
        if(lanelet.adj_left is not None):
            adf_left: "Lanelet" = self.scenario.lanelet_network.find_lanelet_by_id(lanelet.adj_left)
            if (adf_left not in exclude_list):
                list_lanelets.append(adf_left)
        if(lanelet.adj_right is not None):
            adj_right: "Lanelet" = self.scenario.lanelet_network.find_lanelet_by_id(lanelet.adj_right)
            if (adj_right not in exclude_list):
                list_lanelets.append(adj_right)

        # update lists
        exclude_list.append(lanelet)

        return list_lanelets, exclude_list




    def generate_multi_agent_scenario(self,
                                      id_attacker: List[int]=None,
                                      only_consider_dyn_obstacles_that_can_reach_original_planning_problem: bool = True,
                                      only_consider_dyn_obstacles_that_exist_at_first_time_step: bool = True,
                                      only_consider_attackers_in_radius: float = None
                                      ) -> str:
        """
        Generates multi-agent scenario from single-agent scenario and defines attacking vehicles as planning problems.
        Returns scenario id as string.


        Default Settings
        ----------------------

        Per default, only dynamic obstacles are considered that can reach the initial position of the original planning
        problem.

        Per default, only dynamic obtacles that exists at the first time step are considered.

        Optionally activatible: only attackers that start in a certain radius of the original planning problem are used
        """
        scenario, planning_problem_set_original = CommonRoadFileReader(self.file_path).open()
        self.scenario = scenario
        self.planning_problem_set_original = planning_problem_set_original

        dyn_obstacle_list: list = copy.copy(scenario.dynamic_obstacles)
        planning_problem_set: PlanningProblemSet = copy.copy(planning_problem_set_original)

        for k, v in planning_problem_set.planning_problem_dict.items():
            original_planning_problem: "PlanningProblem" = v
            break

        added_planning_problem_cnt: int = 0
        deleted_dynamic_obstacles = list()
        for dynamic_obstacle in dyn_obstacle_list:
            # control flow if obstacle ids are given
            if(id_attacker is not None):
                if(dynamic_obstacle.obstacle_id not in id_attacker):
                    continue

            # check if the dynamic obstacle can intersect with the original planning problem
            if(only_consider_dyn_obstacles_that_can_reach_original_planning_problem):
                if (not self._check_planning_problem_dyn_obstacle_on_connected_road_parts(original_planning_problem, dynamic_obstacle)):
                    continue

            # check if dynamic obstacle exists at first time step
            if(only_consider_dyn_obstacles_that_exist_at_first_time_step):
                if(dynamic_obstacle.initial_state.time_step > 1):
                    continue

            # only consider in specific radius
            if(only_consider_attackers_in_radius is not None):
                if(not isinstance(only_consider_attackers_in_radius, float)):
                    raise ValueError(f'[MultiAgentScenarioGenerator] radius must be float but is {type(only_consider_attackers_in_radius)}')
                elif(only_consider_attackers_in_radius <= 0):
                    raise ValueError(f'[MultiAgentScenarioGenerator] radius must be > 0 but is {only_consider_attackers_in_radius}')
                else:
                    if(not self._check_dyn_obstacle_init_near_planning_problem_init(original_planning_problem,
                                                                                    dynamic_obstacle,
                                                                                    only_consider_attackers_in_radius)):
                        continue


            # Generate initial state from dynamic obstacle
            initial_state: InitialState = dynamic_obstacle.initial_state
            initial_state.yaw_rate = 0.0
            initial_state.slip_angle = 0.0

            # Generate goal state from dynamic obstacle -> copied from dataset converter
            goal_state: CustomState = copy.copy(dynamic_obstacle.prediction.trajectory.final_state)
            goal_position = Rectangle(
                length=dynamic_obstacle.obstacle_shape.length + 2.0,
                width=max(dynamic_obstacle.obstacle_shape.width + 1.0, 3.5),
                center=goal_state.position,
                orientation=goal_state.orientation,
            )
            goal_region = GoalRegion(
                [
                    CustomState(
                        position=goal_position,
                        time_step=Interval(goal_state.time_step-1, goal_state.time_step,)
                    )
                ]
            )


            # Add to planning_problem_set
            planning_problem = PlanningProblem(
                dynamic_obstacle.obstacle_id + 100000,
                initial_state,
                goal_region,
            )

            planning_problem_set.add_planning_problem(planning_problem)
            added_planning_problem_cnt += 1

            # add to delete obstacles
            deleted_dynamic_obstacles.append(dynamic_obstacle.obstacle_id)


        # sanity check
        if(len(dyn_obstacle_list) > 0 and added_planning_problem_cnt == 0):
            raise ValueError(f'[MultiAgentScenarioGenerator] Could not create a single attacking vehicle from scenario. '
                             f'Did you choose the radius {only_consider_attackers_in_radius} too small?')

        # delete obstacles that are a planning problem now
        for obstacle_id in deleted_dynamic_obstacles:
            obstacle = scenario.obstacle_by_id(obstacle_id)
            scenario.remove_obstacle(obstacle)


        # save modified scenario
        scenario.scenario_id.cooperative = True
        cr_file_writer = CommonRoadFileWriter(
            scenario, planning_problem_set, file_format=FileFormat.XML
        )

        output_path = os.path.join(self.output_dir, str(scenario.scenario_id) + ".xml")
        print(f'writing multi-agent scenario to: {output_path}')


        cr_file_writer.write_to_file(
            output_path,
            OverwriteExistingFile.ALWAYS
        )

        # return name of scenario (--> C- Stands for custom dataset)

        return str(scenario.scenario_id)







if __name__ == "__main__":
    scenario_name = "ZAM_Merge-1_1_T-1"



    input_dir: str = ("/home/tmasc/i4driving/critical_scenarios/i4driving/i4crit/scenario_data_root/single_agent_scenarios/")
    output_dir: str = "/home/tmasc/i4driving/critical_scenarios/i4driving/i4crit/scenario_data_root/scenarios/"


    name = generate_multi_agent_scenario(scenario_name, input_dir=input_dir, output_dir=output_dir)
