
import math

from commonroad.scenario.obstacle import ObstacleType, DynamicObstacle
from commonroad.geometry.shape import Rectangle
from commonroad.scenario.state import CustomState, InitialState
from commonroad.scenario.trajectory import Trajectory
from commonroad.prediction.prediction import TrajectoryPrediction

# Typing
from typing import List
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from data_structures.configuration.scenario_configuration import ScenarioConfiguration
    import numpy as np
    from data_structures.agent_vehicle import AgentVehicle



#############################################################
#  How to use
# -------------
# 1. Create an initial state with the AgentInitialStateFactory
# 2. For each state, create a custom state with the AgentStateFactory
# 3. Use the created objects as input for the AgentDynamicObstacleFactory
# 4. Modify scenario of VUT
#
#
#
#
#
############################################################





class AgentStateFactory:
    """
    Factory for State objects for virtual dynamic obstacles of agents.
    """

    """
    Exemplary state definition

    <state>
        <position>
          <point>
            <x>3.1999</x>
            <y>3.5</y>
          </point>
        </position>
        <orientation>
          <exact>3.14</exact>
        </orientation>
        <time>
          <exact>48</exact>
        </time>
        <velocity>
          <exact>16.0</exact>
        </velocity>
     </state>
    """

    @classmethod
    def create_state_from_cartesian(cls, position: "np.array",
                              v_x: float, v_y: float,  a_x: float, a_y: float,
                              time_step: float,
                              vehicle: "AgentVehicle") -> CustomState:
        """
        Creates commonroad CustomState object from cartesian trajectory state.
        """


        velocity: float = math.sqrt(v_x**2 + v_y**2)
        acceleration: float = math.sqrt(a_x**2 + a_y**2)
        # orientation_rad: float = curv_cart.get_orientation_cartesian_from_cartesian(vehicle, position[0], position[1])
        orientation_rad = math.atan2(v_y, v_x)


        state_dict = {
            "position": position,
            "orientation": orientation_rad,
            "time_step": time_step,
            "velocity": velocity,
            "acceleration": acceleration
        }


        return CustomState(**state_dict)


class AgentInitialStateFactory:
    """
    Class that produces initial states for dynamic obstacles created from agent vehicles
    """
    """
    Exemplary Initial state
        <initialState>
      <time>
        <exact>0</exact>
      </time>
      <position>
        <point>
          <x>40.75</x>
          <y>39.2574</y>
        </point>
      </position>
      <orientation>
        <exact>-1.5707</exact>
      </orientation>
      <velocity>
        <exact>15.7864</exact>
      </velocity>
      <acceleration>
        <exact>-1.1849</exact>
      </acceleration>
      <yawRate>
        <exact>0.0</exact>
      </yawRate>
      <slipAngle>
        <exact>0.0</exact>
      </slipAngle>
    </initialState>
    """

    @classmethod
    def create_initial_state_from_cartesian(cls, position: "np.array",
                              v_x: float, v_y: float,  a_x: float, a_y: float,
                              time_step: float, vehicle: "AgentVehicle") -> InitialState:
        """
        Creates Commonroad inital state objet from cartesian trajectory.
        """

        velocity: float = math.sqrt(v_x**2 + v_y**2)
        acceleration: float = math.sqrt(a_x**2 + a_y**2)
        #orientation_rad: float = curv_cart.get_orientation_cartesian_from_cartesian(vehicle, position[0], position[1])
        orientation_rad = math.atan2(v_y, v_x)

        state_dict = {
            "position": position,
            "orientation": orientation_rad,
            "time_step": time_step,
            "velocity": velocity,
            "acceleration": acceleration
        }


        return InitialState(**state_dict)


class AgentDynamicObstacleFactory:
    """
    Factory for modified Dynamic Obstacle instances for agent vehicles.
    """
    virtual_id_offset: int = 30000


    @classmethod
    def create_dynamic_obstacle(cls, vut_config: "ScenarioConfiguration",
                                initial_state: InitialState,
                                list_trajectory_states: List[CustomState],
                                car_id: int):
        """
        Creates a virtual dynamic obstacle for all other agents then the current planning problem.
        The dynamic obstacles are used for inter-agent traffic rules.
        """
        id = car_id + cls.virtual_id_offset

        # Some useful stuff from the commonroad hell-hole
        width = vut_config.vehicle.ego.width
        length = vut_config.vehicle.ego.length
        center = initial_state.position
        orientation = initial_state.orientation


        # Create shape class for vehicle
        obst_rect = Rectangle(length, width, center, orientation)

        # create prediction, whatever that is....?
        prediction = cls._create_trajectory_prediction(initial_state, list_trajectory_states, obst_rect)

        # create obstacle class for vehicle
        obstacle = DynamicObstacle(obstacle_id=id,
                                   obstacle_type=ObstacleType("car"),
                                   obstacle_shape=obst_rect,
                                   initial_state=initial_state,
                                   prediction=prediction)


        # set initial state of obstacle
        obstacle.initial_state = initial_state

        return obstacle


    @classmethod
    def _create_trajectory_prediction(cls, initial_state: InitialState,
                                    list_trajectory_states: List[CustomState],
                                    shape: Rectangle):

        list_all_states = [initial_state]
        list_all_states.extend(list_trajectory_states)

        # Generate Trajectory class
        trajectory = Trajectory(list_all_states[0].time_step, list_all_states)

        # Generate TrajectoryPrediction object. Whatever the difference is to a Trajectory object....
        # TODO: check if this is correct
        rotated_shape = shape.rotate_translate_local(-initial_state.position, -initial_state.orientation)
        prediction = TrajectoryPrediction(trajectory, rotated_shape)

        return prediction
