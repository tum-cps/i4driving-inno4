import numpy as np
from shapely import Polygon
import math

# Typing
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from data_structures.agent_vehicle import AgentVehicle
    from commonroad_dc.pycrccosy import CurvilinearCoordinateSystem
from typing import List, Tuple, Dict





def convert_point_curvillinear_to_cartesian(vehicle: "AgentVehicle", longitudinal: float, lateral: float) -> np.ndarray:
    """
    Convert a point from vehicle-specific curvilinear coordinate system to global cartesian coordinate frame
    param: CLCS -> from vehicle.config.planning.CLCS
    param: longitudinal -> longitudinal coordinate of point
    param: lateral -> lateral coordinate of the point
    """
    CLCS: "CurvilinearCoordinateSystem" = vehicle.config.planning.CLCS
    point_converted: np.ndarray = CLCS.convert_to_cartesian_coords(longitudinal, lateral)
    return point_converted


def convert_point_cartesian_to_curvilinear(vehicle: "AgentVehicle", x: float, y: float) -> np.ndarray:
    """
    Convert a point from global cartesian coordinate frame to local curviliniear coordinate frame
    param: CLCS -> from vehicle.config.planning.CLCS
    param: x -> x coordinate of point
    param: y -> y coordinate of the point
    """
    CLCS: "CurvilinearCoordinateSystem" = vehicle.config.planning.CLCS
    point_converted: np.ndarray = CLCS.convert_to_curvilinear_coords(x, y)
    return point_converted




def convert_velocity_vector_curvilinear_to_cartesian(vehicle: "AgentVehicle", p_lon: float,  v_lon: float, v_lat: float) -> np.ndarray:
    """
    Converts a velocity vector from curvilinear to cartesian using the angle of the tangent.
    """
    CLCS: "CurvilinearCoordinateSystem" = vehicle.config.planning.CLCS
    tangent_cart = CLCS.tangent(p_lon)

    alpha: float= math.atan2(tangent_cart[1], tangent_cart[0])

    v_x: float = math.cos(alpha) * v_lon - math.sin(alpha) * v_lat
    v_y: float = math.sin(alpha) * v_lon + math.cos(alpha) * v_lat

    return np.asarray([v_x, v_y])



def convert_velocity_vector_cartesian_to_curvilinear(vehicle: "AgentVehicle", x: float, y: float, v_x: float, v_y: float) -> np.ndarray:
    """
    Converts a velocity vector from cartesian to curvilinear using the angle of the tangent.
    """
    CLCS: "CurvilinearCoordinateSystem" = vehicle.config.planning.CLCS

    # find corresponding p_lon
    point_converted = convert_point_cartesian_to_curvilinear(vehicle, x, y)
    p_lon = point_converted[0]

    # compute forward angle alphay cart -> cvl
    tangent_cart = CLCS.tangent(p_lon)
    alpha: float= math.atan2(tangent_cart[1], tangent_cart[0])

    # special cases to avoid dividing by zero:
    if(np.isclose(alpha, 0)):
        v_lon = v_x
        v_lat = 0
    elif(np.isclose(math.pi/2, 0)):
        v_lon = 0
        v_lat = v_x
    else:
    # normal case
        # use forward angle to compute backward
        v_lon: float = (v_x/math.cos(alpha)) + (v_y/math.cos(math.pi/2-alpha))
        v_lat: float = (-v_x/math.cos(math.pi/2-alpha)) + (v_y/math.cos(alpha))

    return np.asarray([v_lon, v_lat])



def convert_accelereration_curvilinear_to_cartesian(vehicle: "AgentVehicle", p_lon: float,  a_lon: float, a_lat: float) -> np.ndarray:
    """
    Convert acceleration from vehicle-specific curvilinear to global cartesian coordinate system.
    """
    # same mathematical principle than for velocity
    return convert_velocity_vector_curvilinear_to_cartesian(vehicle, p_lon, a_lon, a_lat)


def convert_accelleration_cartesian_to_curvilinear(vehicle: "AgentVehicle", x: float, y: float, a_x: float, a_y: float) -> np.ndarray:
    """
    Convert acceleration from global cartesian to vehicle-specific curvilinear coordinate system
    """
    # same mathematical principle than for velocity
    return convert_velocity_vector_cartesian_to_curvilinear(vehicle, x, y, a_x, a_y)









def convert_shapely_polygon_cartesian_to_curvilinear(vehicle: "AgentVehicle", polygon: Polygon) -> Polygon:
    """
    Converts shapely polygon from cartesian to curvilinear
    """
    CLCS = vehicle.config.planning.CLCS

    x_min: float = polygon.bounds[0]
    y_min: float = polygon.bounds[1]
    x_max: float = polygon.bounds[2]
    y_max: float = polygon.bounds[3]

    vertex1 = CLCS.convert_to_curvilinear_coords(x_min, y_min)
    vertex2 = CLCS.convert_to_curvilinear_coords(x_max, y_min)
    vertex3 = CLCS.convert_to_curvilinear_coords(x_max, y_max)
    vertex4 = CLCS.convert_to_curvilinear_coords(x_min, y_max)

    polygon_curv = Polygon([vertex1, vertex2, vertex3, vertex4])


    return polygon_curv



def convert_shapely_polygon_curvilinear_to_cartesian(vehicle: "AgentVehicle", polygon: Polygon) -> Polygon:
    """
    Converts shapely polygon from curvilinear to cartesian
    """
    CLCS = vehicle.config.planning.CLCS

    p_lon_min: float = polygon.bounds[0]
    p_lat_min: float = polygon.bounds[1]
    p_lon_max: float = polygon.bounds[2]
    p_lat_max: float = polygon.bounds[3]

    vertex1 = CLCS.convert_to_cartesian_coords(p_lon_min, p_lat_min)
    vertex2 = CLCS.convert_to_cartesian_coords(p_lon_max, p_lat_min)
    vertex3 = CLCS.convert_to_cartesian_coords(p_lon_max, p_lat_max)
    vertex4 = CLCS.convert_to_cartesian_coords(p_lon_min, p_lat_max)

    polygon_cart = Polygon([vertex1, vertex2, vertex3, vertex4])


    return polygon_cart





def get_coordinate_system_manager_of_vehicle(vehicle: "AgentVehicle") -> "CurvilinearCoordinateSystem":
    """
    Returns the cordinate system manager instance (CLCS) of the vehicle.
    """
    return vehicle.config.planning.CLCS



def get_reference_path_of_vehicle_in_cartesian(vehicle: "AgentVehicle") -> List[np.ndarray]:
    """
    Return reference path in cartesian as polyline list of points.
    """
    CLCS: "CurvilinearCoordinateSystem" = vehicle.config.planning.CLCS
    reference_path: List[np.ndarray] = CLCS.reference_path_original()
    return reference_path


def get_orientation_cartesian_from_curvilinear(vehicle: "AgentVehicle", p_lon: float) -> float:
    """
    Returns rad valued angle in cartesian from curvilinear point.
    Orientation approximated via tangent on p_lon axis.
    """
    CLCS: "CurvilinearCoordinateSystem" = vehicle.config.planning.CLCS
    tangent_cart = CLCS.tangent(p_lon)
    alpha: float = math.atan2(tangent_cart[1], tangent_cart[0])
    return alpha


def get_orientation_cartesian_from_cartesian(vehicle: "AgentVehicle", x: float, y: float) -> float:
    """
    Returns rad valued angle in cartesian from cartesian point.
    Orientation approximated via tangent on p_lon axis.
    """
    CLCS: "CurvilinearCoordinateSystem" = vehicle.config.planning.CLCS

    # find corresponding p_lon
    point_converted = convert_point_cartesian_to_curvilinear(vehicle, x, y)
    p_lon = point_converted[0]

    # compute forward angle alphay cart -> cvl
    tangent_cart = CLCS.tangent(p_lon)
    alpha: float= math.atan2(tangent_cart[1], tangent_cart[0])
    return alpha


def get_velocity_cartesian_from_cartesian_orientation(v_total: float, orientation_rad: float) -> Tuple[float, float]:
    """
    Takes total velocity and orientation(rad) with respect to the cartesian x-axis and returns v_x and v_y in cartesian.
    """
    v_x = math.acos(orientation_rad) * v_total
    v_y = math.asin(orientation_rad) * v_total
    return (v_x, v_y)