import copy

from commonroad.scenario.obstacle import DynamicObstacle

# Typing
from data_structures.agent_vehicle import AgentVehicle
from data_structures.configuration.scenario_configuration import ScenarioConfiguration




def temporarily_modify_config(vehicle: AgentVehicle, t_start: int, t_end: int,
                              obstacle_offset: int = 30000) -> ScenarioConfiguration:
    """
    Changes values t_start and t_end values for one vehicle.
    Returns old config for later reversion
    """
    old_config = copy.deepcopy(vehicle.config)


    vehicle.config.planning.step_start = t_start
    vehicle.planning_problem.initial_state.time_step = t_start
    vehicle.config.planning.steps_computation = t_end - t_start
    vehicle.reach_interface._reach.step_start = t_start
    vehicle.reach_interface._reach.step_end = t_end


    # set old dynamic obstacles as virtual, so they dont interfere again with new iteration
    temp = copy.copy(vehicle.config.scenario._dynamic_obstacles)
    for key, dynamic_obstacle in temp.items():
        if(dynamic_obstacle.obstacle_id > obstacle_offset):
            setattr(dynamic_obstacle, 'is_virtual_agent', True)

    # for astar collision checker
    for obstacle in vehicle.config.scenario.obstacles:
        if(isinstance(obstacle, DynamicObstacle)):
            setattr(obstacle, 'is_virtual_agent', True)
            setattr(obstacle, 'is_outdated', True)


    return old_config



def revert_modified_steps(vehicle: AgentVehicle, old_configuration: ScenarioConfiguration) -> None:
    """
    Reverts steps given an old configuration.
    """
    vehicle.config.planning.step_start = old_configuration.planning.step_start
    vehicle.config.planning.steps_computation = old_configuration.planning.steps_computation
    vehicle.reach_interface._reach.step_start = old_configuration.planning.step_start
    vehicle.reach_interface._reach.step_end = old_configuration.planning.step_start + old_configuration.planning.steps_computation