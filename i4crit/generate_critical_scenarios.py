import time
import os


# Own Code Base
from data_structures.configuration.configuration_builder import ConfigurationBuilder
from data_structures.agent_vehicle import AgentVehicle
from data_structures.scenario_manager import ScenarioManager
from utils.scenario_modification.planning_problem_generation import MultiAgentScenarioGenerator
from utils.time_step_computation.time_step_computation import TimeStepGenerator
from visualization.scenario_visualization import plot_scenario

# Typing
from typing import List


def main():
    ############### PARAMS #############
    # name_scenario = "ZAM_Intersection-1_2_T-1"                # intersection, left-before-right
    name_scenario = "ZAM_MergeMultiagent"                             # merge with 2 attackers
    # name_scenario = "ZAM_Merge-1_1_T-1"                         # merge with 1 attacker

    # name_scenario = "C-DEU_B471-2_1"                            # round about
    # name_scenario = "C-DEU_MerzenichRather-2_8814400_T-14549"

    # name_scenario = "C-DEU_CologneKlettenberg-1_680_T-149"


    # name_scenario = "C-DEU_B471-1_1_T-1"                      # road block
    # name_scenario = "C-DEU_OnlyRoad"                # road without road block


    path_root = os.getcwd() + "/scenario_data_root"

    t_end: int = 50


    ##################################


    # generate multi-agent-scenario
    name_scenario = MultiAgentScenarioGenerator(scenario_name=name_scenario).generate_multi_agent_scenario(
        id_attacker=None,
        only_consider_dyn_obstacles_that_can_reach_original_planning_problem=True,
        only_consider_dyn_obstacles_that_exist_at_first_time_step=True,
        only_consider_attackers_in_radius=None
    )


    # load list of configs, each element containing one planning problem
    list_configs = ConfigurationBuilder.build_configurations(name_scenario, path_root,
                                                             consider_inter_agent_traffic_rules=True)


    list_vehicles = [AgentVehicle(config) for config in list_configs]

    # Set one vehicle as target
    vut_vehicle = list_vehicles[0]
    vut_vehicle.set_as_vut()

    # Create scenario manager
    scenario_manager = ScenarioManager(list_vehicles)


    # compute steps automatically via heuristic.
    # change first_offset if you run into problems.
    list_steps: List[int] = TimeStepGenerator.generate_time_steps_from_heuristic_intersecting_ref_paths(list_vehicles,
                                                                                                        t_end=t_end,
                                                                                                        first_offset=8,
                                                                                                        step_size=12)
    t_total_end: int = list_steps[-1]



    t_0 = time.perf_counter()
    for idx, step_start in enumerate(list_steps[:-1]):

        #for vehicle in list_vehicles:
            #plot_scenario_with_projection_domain(vehicle.reach_interface)


        step_end: int = list_steps[idx+1]


        # Propagate reachable set
        scenario_manager.perform_reachability_analysis(t_start=step_start, t_end=step_end)


        # Find overlap
        for step in range(step_start, step_end + 1):
            scenario_manager.find_overlap_in_reachable_sets(step=step)


        # propagate planning
        scenario_manager.perform_planning(t_start=step_start, t_end=step_end, vut_vehicle=vut_vehicle)


        # get input from vut vehicle
        scenario_manager.get_input_from_vut_vehicle(vut_vehicle, step_start, step_end)


        # check if a collision happened
        _, _ = scenario_manager.check_collision_of_vut(step_start, step_end)



    print(f'Scenario took: {time.perf_counter() - t_0}s and has {(t_total_end - list_steps[0]) * list_configs[0].planning.dt}s')


    # Plot
    plot_scenario(list_vehicles, list_step_start=list_steps, step_end=t_total_end,
                  dict_step_to_collisions=scenario_manager.dict_step_to_collisions)



if __name__ == "__main__":
    main()
